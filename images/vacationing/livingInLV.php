<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<!--  the following two lines load the jQuery library and JavaScript files -->
	<script src="<?php echo $jqueryloc; ?>" type="text/javascript"></script>
    <script src="<?php echo $jqfile; ?>" type="text/javascript"></script>
	
	<!-- include Google's AJAX API loader -->
	<script src="http://www.google.com/jsapi"></script>
	<!-- load JQuery and UI from Google (need to use UI to animate colors) -->
	<script type="text/javascript">
	google.load("jqueryui", "1.5.2");
	</script>
	<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script-->
	<title>Living in Vegas</title>
	
	<script type="text/javascript">
		function JQFunctions()
				{
				    var vacationingPhotos = new Array();
					var localRecreationPhotos = new Array();
					var funInTheParkPhotos = new Array();
					var localAmusementPhotos = new Array();
					var tennisTownPhotos = new Array();
				    <?php 
						if (isset($vacationingPhotos))
							   foreach($vacationingPhotos as $photo)
									{ ?>
										vacationingPhotos.push("<?php echo base_url($photo->link);?>");
					<?php	} 
							
						if (isset($localRecreationPhotos))
								 foreach($localRecreationPhotos as $photo)
									{ ?>
										localRecreationPhotos.push("<?php echo base_url($photo->link);?>");
					<?php	} 
						if (isset($funInTheParkPhotos))
								 foreach($funInTheParkPhotos as $photo)
									{ ?>
										funInTheParkPhotos.push("<?php echo base_url($photo->link);?>");
					<?php	} 	
						if (isset($localAmusementPhotos))
								 foreach($localAmusementPhotos as $photo)
									{ ?>
										localAmusementPhotos.push("<?php echo base_url($photo->link);?>");
					<?php	} 		
						if (isset($tennisTownPhotos))
								 foreach($tennisTownPhotos as $photo)
									{ ?>
										tennisTownPhotos.push("<?php echo base_url($photo->link);?>");
					<?php	} 	
							?>
						
					//alert(vacationingPhotos[0]);
					
					$('.playerBase').css('display','none');
					$('.playerBase2').css('display','block');
					$('.container')
						.animate({top:"+=50"},2000);
					
					setInterval(function(){
					
							// Sun visibly going across sky	
							$('.sun')
								.animate({opacity:1.0,
										  top:"30"},2500)
								.animate({opacity:1.0,
										  left:"+=90",
										  zoom:"-=50%",
										  top:"-=22"},1500)
								.animate({opacity:1.0
										  },1000)		  
								.animate({opacity:1.0,
										  left:"+=90",
										  zoom:"+=50%",
										  top:"+=22"},1500)
								.animate({opacity:0.0},2500)
								.animate({opacity:0.0},1000)
								.css('display','block');
								
								
                             // Background scene of street
							$('.scene')
								.animate({opacity:0.8},2500)
								.animate({opacity:0.7},1500)
								.animate({opacity:0.7},1000)
								.animate({opacity:0.8},1500)
								.animate({opacity:0.9},2500)
								.animate({opacity:1.0},1000)
								.animate({opacity:0.9},2500)
								.animate({opacity:0.9},1500)
								.animate({opacity:0.9},1000)
								.animate({opacity:0.9},1500)
								.animate({opacity:1.0},2500)
								.animate({opacity:1.0},1000);
							
							
							// Sun invisibly retracing across sky
							$('.sun')
								.animate({opacity:0.0,
										  top:"30"},2500)
								.animate({opacity:0.0,
										  left:"-=90",
										  top:"-=22"},1500)
								.animate({opacity:0.0
								          },1000)		  
								.animate({opacity:0.0,
										  left:"-=90",
										  top:"+=22"},1500)
								.animate({opacity:0.0},2500)
								.animate({opacity:0.0},1000)
								.css('display','block');
								
							// Moon invisibly doing initial trace across sky	
							$('.moon')
								.animate({opacity:0.0,
										  top:"-10"},2500)
								.animate({opacity:0.0,
										  left:"+=90",
										  top:"-=22"},1500)
								.animate({opacity:0.0},1000)		  
								.animate({opacity:0.0,
										  left:"+=90",
										  top:"+=22"},1500)
								.animate({opacity:0.0},2500)
								.animate({opacity:0.0},1000)
								.css('display','block');	
								
							// Moon visibly retracing across sky
							$('.moon')
								.animate({opacity:1.0,
										  top:"-10"},2500)
								.animate({opacity:1.0,
										  left:"-=90",
										   zoom:"-=50%",
										  top:"-=22"},1500)
								.animate({opacity:1.0},1000)		  
								.animate({opacity:1.0,
										  left:"-=90",
										   zoom:"+=50%",
										  top:"+=22"},1500)
								.animate({opacity:0.0},2500)
								.animate({opacity:0.0},1000)
								.css('display','block');
	
							$('.kidsPlay')
								.animate({opacity:0.0,
										  left:"+=2",
										  top:"-=2"
										  },1000)
								.animate({opacity:1.0,
										  left:"+=2",
										  top:"-=2"
										  },1000)
								.animate({opacity:1.0,
								          left:"-=2",
										  top:"+=2",
										  },2000)
								.animate({opacity:1.0,
										  left:"+=2",
										  top:"-=2"
										  },2000)
								.animate({opacity:1.0,
										  left:"-=2",
										  top:"+=2"
										  },2000)		  
								.animate({opacity:0.0},1000)
								.animate({opacity:0.0},11000);
								
				/*			$('.scene').animate({opacity:1.0},2000,
											function() {
														 $(this).css('border','25px solid #FFFFCC');
														 $(this).css('border-radius','15px');
													   })
											.animate({opacity:1.0},2000,		   
											function() {
														 $(this).css('border','25px solid #FFCCCC');
														 $(this).css('border-radius','15px');
													   })
										    .animate({opacity:1.0},2000,
											function() {
														 $(this).css('border','25px solid #FF9966');
														 $(this).css('border-radius','15px');
													   })
											.animate({opacity:1.0},2000,
											function() {
														 $(this).css('border','25px solid #CC9966');
														 $(this).css('border-radius','15px');
													   })
										    .animate({opacity:1.0},2000,
											function() {
														 $(this).css('border','25px solid #CC9933');
														 $(this).css('border-radius','15px');
													   })
										    .animate({opacity:1.0},2000,
											function() {
													 $(this).css('border','25px solid #996633');
														 $(this).css('border-radius','15px');
													   });
													   
													   
							
											   */
							
						},50);
						
			        var introMessages = new Array();
					introMessages[0] =  "<p class='clkmess' style='font-size:1.2em;color:green'>FAMILY OPTIONS IN LAS VEGAS</p><br/>" +
										"<p>Finding out options for Family Living in Las Vegas can be a challenge.  The cultural options for family enjoyment are not obvious at first. " +
					                    "But some discovery over the years has provided my family some satifactory options for Good Clean Fun!</p>" +
										"<br/><p style='font-size:1.2em'>SCROLL to Find Out More</p>";

					introMessages[1] = "<p class='clkmess' style='font-size:1.2em;color:green'>LOCAL RECREATION</p><br/>" +
									   "<p>One Discovery is that Las Vegas, NV is a Desert Recreational Area which provides several month's opportunity for " +
									   "hiking climbing, biking, sking, camping, and more in many nearby mountainous areas.  <span class='hlight'><a style='color:brown' href='http://www.redrockcanyonlv.org/redrockcanyon/recreation/'>Redrock Canyon Recreational</a></span> and" +
									   "<span class='hlight'><a style='color:red' href='http://parks.nv.gov/parks/valley-of-fire-state-park/'> Valley of Fire</a></span> provide great exercise and site-seeing " +
									   "during the fairer weather months.  <span class='hlight'><a style='color:green' href='http://www.gomtcharleston.com/'>Mount Charleston</a></span> is best during the hot Vegas mid-summer as you escape 110-115 degrees " +
									   "for 80-90 degrees, only 30 minutes away!  Where you find camping, resort lodging, dining, and more.  Mount Charleston actually has a ski slope, lodges, and slay rides for " +
									   " winter fun!</p>";
					introMessages[2] = "<p class='clkmess'  style='font-size:1.2em;color:green'>VACATIONING</p><br/>" +
									   "Las Vegas is also is a great location to travel away to surrounding states for Vacation and Recreation. " +
					                   "California, Arizona, and Utah have very nice destinations.  My families favorites have been:  <a style='color:red' href='https://disneyland.disney.go.com/'>Disneyland and Disneyland Adventure Parks</a>, " +
									   "<a style='color:green' href='http://www.legoland.com/'>LegoLand</a>, <a style='color:blue' href='http://seaworldparks.com/en/seaworld-sandiego/'>SeaWorld</a>, <a style='color:purple' href='http://www.universalstudioshollywood.com/'>Universal Studios Hollywood</a>, " +
									   "<a style='color:#5F9EA0' href='https://www.knotts.com/'>Knotts Berry Farm</a>, " +
									   "<a style='color:#DAA520' href='http://www.smgov.net/portals/beach/'>Santa Monica State Beach</a>, <a style='color:#800000' href='http://www.balboapark.org/'>Balboa Park</a>, <a style='color:#B0E0E6' href='http://www.catalinachamber.com/'>Catalina Island</a>, " +
									   "<a style='color:#F08080' href='www.californiacruises.com/'>cruises to Mexico</a>," +
									   "with these options being in California.  In Arizona:  the <a style='color:#FF8C00' href='http://phoenixzoo.org/'>Phoenix Zoo</a>," +
									   "the <a style='color:#B0C4DE' href='http://www.azscience.org/'>Arizona Science Center</a>, and the <a style='color:#008B8B' href='http://www.visitsedona.com/'>city of Sedona</a>.  " +
									   "In Utah:  <a style='color:#ADFF2F' href='http://www.nps.gov/zion/index.htm'>Zion National Park</a>, and <a style='color:#000080' href='http://www.nps.gov/brca/index.htm'>Bryce Canyon Nat'l Park</a>, both for outstanding, awe-inspiring site-seeing and hiking!";
					introMessages[3] = "<p class='clkmess' style='font-size:1.2em;color:green'>FUN IN THE PARK</p><br/>" +
									   "Las Vegas also has a beautiful and diverse system of parks and swimming pools within the city.  During the Spring, Summer, and Fall months " +
					                   "Vegas has an abundance of events that include a variety of concert music, food, carnival rides, play events for children, movies, swimming, " +
									   "free prizes and other free stuff.  The events are sponsored by city and private organizations.  They are usually organized for family fun at " +
									   "modest cost.  But on many occassions all (or much) is free!  My kids have grown up enjoying several of these events a year.  Event lists for each " +
									   "can be found weekly in the newspaper, seasonally at the <a style='color:#F4A460' href='http://www.clarkcountynv.gov/Depts/parks/Events/Pages/default.aspx'>Clark County Parks Events website</a>, or you can find many private listings of notices and calendars online " +
									   "(i.e. <a  style='color:#708090' href='http://www.vegas4locals.com/lasvegasevents.html'>Las Vegas 4locals</a>) if you Google Search.";
					introMessages[4] = "<p class='clkmess' style='font-size:1.2em;color:green'>LOCAL AMUSEMENT</p><br/>" +
									   "Las Vegas has a popular set of amusement park and arcade attractions.  <a style='color:#8B0000' href='http://www.chuckecheese.com/'>Chucky E. Cheese's</a> is a combination of restaurant, arcade, and amusement park features that particularly draws parents who are " +
									   "kids at heart, along with their kids. <a style='color:#696969' href='http://www.lvmgp.com/'>Las Vegas Mini Gran Prix</a> has an arcade inside and an amusement park outside that includes one fast go-Kart track, and another faster, larger-scale race car track."; 
					
                    introMessages[5] = "<p class='clkmess' style='font-size:1.2em;color:green'>TENNIS TOWN</p><br/>" +
									   "Las Vegas is also a tennis town with a favorable year-round climate and a great many tennis training facilities that cater to children and adults.  The <a  style='color:#0000FF' href='http://tennislink.usta.com/Dashboard/Main/'>USTA</a> has a strong presence in town with multiple" +
					                   " tournaments hosted monthly at various sites for all levels of players.  The great tennis pro Andre Agassi is from Las Vegas and he has endorsed the <a style='color:#DC143C' href='http://www.darlingtenniscenter.net/'>Amanda and Stacy Darling Tennis Center</a> as one of the finest training facilities in the country.  It is a USTA training facility " +
									   "and is located in the Summerlin Park area of the Las Vegas community";
					
					$('#middleScreen').html(introMessages[0]);
					$('#rightScreenContent').html(introMessages[1]);

					var indx = 0;
					var numItems = 0;
					var photoset = 0;
					
					var photoArr = new Array();	
					$('#leftPointer').click(function()
						{
						 $('#rightPointer').css('background-color','transparent');
						 numItems = introMessages.length;
						 
						 indx++;
						  photoset = indx;
						  
						if(photoset == 1)
								photoArr = localRecreationPhotos;
						else if(photoset == 2)
								photoArr = vacationingPhotos;
						else if(photoset == 3)
								photoArr = funInTheParkPhotos;
						else if(photoset == 4)
								photoArr = localAmusementPhotos;
						else if(photoset == 5)
								photoArr = tennisTownPhotos;
					
						
						if(photoArr.length > 0)
							{
								
								
								$('#middlePhoto').html("<img src='"+photoArr[1]+"' alt='Photo' height='200' width='300'>");
								$('#rightPhotoContent').html("<img src='"+photoArr[2]+"' alt='Photo' height='200' width='300'>");
								$('#leftPhotoContent').html("<img src='"+photoArr[0]+"' alt='Photo' height='200' width='300'>");
								
								$('.playerBase').css('display','block');
								$('.playerBase2').css('display','none');
								
								$('#middleScreen').mouseover(function()
									{
										$('.playerBase').css('display','none');
										$('.playerBase2').css('display','block');
									});
							    $('#middleScreen').mouseleave(function()
									{
										$('.playerBase').css('display','block');
										$('.playerBase2').css('display','none');
									});	
							}
						else
							{
								$('.playerBase').css('display','none');
								$('.playerBase2').css('display','block');
								
								$('#middleScreen').bind('mouseover mouseleave',function()
									{
										$('.playerBase').css('display','none');
										$('.playerBase2').css('display','block');
									});	
							
							}
						  
						  
						 $('#leftScreenContent').html(introMessages[indx-1]);
						 if((indx) <= numItems-1)
							{
							if(indx == 0)
								$('#middleScreen').css('color','brown');
							else
								$('#middleScreen').css('color','black');
								
							$('#middleScreen').html(introMessages[indx]);
							if((indx+1) <= numItems-1)
								$('#rightScreenContent').html(introMessages[indx+1]);
							else
								$('#rightScreenContent').html('');
								
							}
						 else if((indx)>(numItems-1))
						    {
								$('#middleScreen').html('');
								$('#leftPointer').css('background-color','#4D4D4D');
								$('#rightPointer').css('background-color','transparent');
								indx = numItems;
							}
						
						});
					
					$('#rightPointer').click(function()
						{
						 $('#leftPointer').css('background-color','transparent');
						 numItems = introMessages.length;
						 
						 indx--;	
						 photoset = indx;
						 
						
						if(photoset == 1)
								photoArr = localRecreationPhotos;
						else if(photoset == 2)
								photoArr = vacationingPhotos;
						else if(photoset == 3)
								photoArr = funInTheParkPhotos;
						else if(photoset == 4)
								photoArr = localAmusementPhotos;
						else if(photoset == 5)
								photoArr = tennisTownPhotos;
					
						
						if(photoArr.length > 0)
							{
							
								$('#middlePhoto').html("<img src='"+photoArr[1]+"' alt='Photo' height='200' width='300'>");
								$('#leftPhotoContent').html("<img src='"+photoArr[0]+"' alt='Photo' height='200' width='300'>");
								$('#rightPhotoContent').html("<img src='"+photoArr[2]+"' alt='Photo' height='200' width='300'>");
								
								$('.playerBase').css('display','block');
								$('.playerBase2').css('display','none');
								
								$('#middleScreen').mouseover(function()
									{
										$('.playerBase').css('display','none');
										$('.playerBase2').css('display','block');
									});
							   $('#middleScreen').mouseleave(function()
									{
										$('.playerBase').css('display','block');
										$('.playerBase2').css('display','none');
									});	
								
								
							}
						else
							{
								$('.playerBase').css('display','none');
								$('.playerBase2').css('display','block');
								
								$('#middleScreen').bind('mouseover mouseleave',function()
									{
										$('.playerBase').css('display','none');
										$('.playerBase2').css('display','block');
									});	
							
							}
						
						 
						 $('#rightScreenContent').html(introMessages[indx+1]);
						 if((indx) >= 0)
							{
							if(indx == 0)
								$('#middleScreen').css('color','brown');
							else
								$('#middleScreen').css('color','black');
								
							$('#middleScreen').html(introMessages[indx]);
							if((indx-1) >= 0)
								$('#leftScreenContent').html(introMessages[indx-1]);
							else
								$('#leftScreenContent').html('');
							
							}
						 else if((indx) < 0)
						    {
								$('#middleScreen').html('');
								$('#rightPointer').css('background-color','#4D4D4D');
								$('#leftPointer').css('background-color','transparent');
								indx = -1;
							}
						
						});
						
					
						
					var indx2 = 0;
				
					
					$('#rightPhotoPointer').click(function()
						{
						
					//	 $('#rightPhotoPointer').css('background-color','transparent');
						 numItems = photoArr.length;
						 photoset = indx2;
						 var scroll = false;
						 indx2--;
						 if((indx2) < 0)
						    {
								indx2 = numItems-1;
								scroll = true;
							}
						
							$('#leftPhotoContent').html("<img src='"+photoArr[indx2]+"' alt='Photo' height='200' width='300'>");
							if(scroll == true)
								indx2 = -1;
							$('#middlePhoto').html("<img src='"+photoArr[indx2+1]+"' alt='Photo' height='200' width='300'>");
							$('#rightPhotoContent').html("<img src='"+photoArr[indx2+2]+"' alt='Photo' height='200' width='300'>");
						
						});
					
					
						
					$('#leftPhotoPointer').click(function()
						{	
							 numItems = photoArr.length;
							 photoset = indx2;
							 indx2++;
							 if((indx2+2) > (numItems-1))
								indx2 = 0;
								
							$('#leftPhotoContent').html("<img src='"+photoArr[indx2]+"' alt='Photo' height='200' width='300'>");
							$('#middlePhoto').html("<img src='"+photoArr[indx2+1]+"' alt='Photo' height='200' width='300'>");
							$('#rightPhotoContent').html("<img src='"+photoArr[indx2+2]+"' alt='Photo' height='200' width='300'>");
						
						});
						
						
						setInterval(function(){
									
									$('.clkmess')	
										.animate({opacity:'0.5'},500)
										.animate({opacity:'1.0'},500);
								},25);
						
						
						
					
				
				}
				
				
				
				
			
	</script>

	
	
</head>
<style type="text/css">
   .topWindow{
				 position:relative;
				 height: 190px;
				 width: 260px;
			     left:35%;
			     zoom:200%;
				 
				  -webkit-transition: border-radius 0.5s ease-in-out;
				  -moz-transition: border-radius 0.5s ease-in-out;
				  -o-transition: border-radius 0.5s ease-in-out;
				  transition: border-radius 0.5s ease-in-out;
				  
				
				 margin-top:5%;
				 -moz-transition: all 2s;
				-webkit-transition: all 2s;
				transition: all 2s;
				
				
   }

   .scene{  
				position:relative;
				height:100%;
			    width:100%;
				z-index:2;
				left:-7%;
				margin-top:90px;
				margin-bottom:30px;
				
				-webkit-transform: scale(1.5,1.5);
				-moz-transform: scale(1.5,1.5);
				transform: scale(1.5,1.5);
				
				 border: 35px solid #CC9966;
				 border-radius: 15px;
				-moz-border-radius: 15px;
				-webkit-border-radius: 15px;
			

	}
			
		
		
	
	
    .scene{
			   
			   background-image: url(<?php echo $street; ?>);
	}
	
	

	  .shade{
		position:absolute;
		display:block;
		height: 100%;
		width: 100%;
		z-index:1;
		opacity:0.75;
		background: rgba(0,0,0,.9);
		
	}
	
	
	
	.sun{
		position:relative;
		height:12%;
		width:12%;
		top:30px;
		left:4%;
		background-color:orange;
		border:8px solid orange;
		border-radius:48%;
		z-index:2;
		opacity:0.0;
	
	}
	
	.moon{
		position:relative;
		height:12%;
		width:12%;
		left:4%;
		top:30px;
		background-color:white;
		border:8px solid white;
		border-radius:48%;
		z-index:2;
		opacity:0.0;
	    }
		
		
	.birdFly1{
		position:absolute;
		height:50%;
		width:50%;
		top:5px;
		left:75%;
		z-index:4;
		opacity:0.0;
		-webkit-transform: scale(0.25);
		-moz-transform: scale(0.25);
		transform: scale(0.25);
		-webkit-animation: movebirds 25s linear infinite;
		-moz-animation: movebirds 25s linear infinite;
		-o-animation: movebirds 25s linear infinite;
		
		-webkit-animation: flaptop 25s linear infinite;
		-moz-animation: flaptop 25s linear infinite;
		-o-animation: flaptop 25s linear infinite;
	    }
		
	.birdFly2{
		position:absolute;
		height:50%;
		width:50%;
		top:5px;
		left:75%;
		z-index:3;
		opacity:0.0;
		-webkit-transform: scale(0.25);
		-moz-transform: scale(0.25);
		transform: scale(0.25);
		-webkit-animation: movebirds 25s linear infinite;
		-moz-animation: movebirds 25s linear infinite;
		-o-animation: movebirds 25s linear infinite;
		
		-webkit-animation: flapbottom 25s linear infinite;
		-moz-animation: flapbottom 25s linear infinite;
		-o-animation: flapbottom 25s linear infinite;
	    }
	
	.secondbirdFly1{
		position:absolute;
		height:50%;
		width:50%;
		top:10px;
		left:75%;
		z-index:6;
		opacity:0.0;
		-webkit-transform: scale(0.20);
		-moz-transform: scale(0.20);
		transform: scale(0.20);
		-webkit-animation: movebirds 26s linear infinite;
		-moz-animation: movebirds 26s linear infinite;
		-o-animation: movebirds 26s linear infinite;
		
		-webkit-animation: flaptop 26s linear infinite;
		-moz-animation: flaptop 26s linear infinite;
		-o-animation: flaptop 26s linear infinite;
	    }
		
	.secondbirdFly2{
		position:absolute;
		height:50%;
		width:50%;
		top:10px;
		left:75%;
		z-index:5;
		opacity:0.0;
		-webkit-transform: scale(0.20);
		-moz-transform: scale(0.20);
		transform: scale(0.20);
		-webkit-animation: movebirds 26s linear infinite;
		-moz-animation: movebirds 26s linear infinite;
		-o-animation: movebirds 26s linear infinite;
		
		-webkit-animation: flapbottom 26s linear infinite;
		-moz-animation: flapbottom 26s linear infinite;
		-o-animation: flapbottom 26s linear infinite;
	    }
	
	.thirdbirdFly1{
		position:absolute;
		height:50%;
		width:50%;
		top:10px;
		left:75%;
		z-index:8;
		opacity:0.0;
		-webkit-transform: scale(0.15);
		-moz-transform: scale(0.15);
		transform: scale(0.15);
		-webkit-animation: movebirds 27s linear infinite;
		-moz-animation: movebirds 27s linear infinite;
		-o-animation: movebirds 27s linear infinite;
		
		-webkit-animation: flaptop 27s linear infinite;
		-moz-animation: flaptop 27s linear infinite;
		-o-animation: flaptop 27s linear infinite;
	    }
		
	.thirdbirdFly2{
		position:absolute;
		height:50%;
		width:50%;
		top:10px;
		left:75%;
		z-index:7;
		opacity:0.0;
		-webkit-transform: scale(0.15);
		-moz-transform: scale(0.15);
		transform: scale(0.15);
		-webkit-animation: movebirds 27s linear infinite;
		-moz-animation: movebirds 27s linear infinite;
		-o-animation: movebirds 27s linear infinite;
		
		-webkit-animation: flapbottom 27s linear infinite;
		-moz-animation: flapbottom 27s linear infinite;
		-o-animation: flapbottom 27s linear infinite;
	    }	
	
	
		
	.kidsPlay{
		position:absolute;
		height:25%;
		width:25%;
		top:53%;
		left:22%;
		z-index:3;
		opacity:1.0;
		-webkit-transform: scale(0.23);
		-moz-transform: scale(0.23);
		transform: scale(0.23);
		
	    }	
	
	   .viewer {position:relative;
				zoom:60%;
				left:32%;

	   }
	   
	   .container {position:relative;top:10%}
	   .displayer {
			position:relative;
			float:left;
			height:217px;
			width:845px;
			top:30%;
			left:-70%;
			padding-left:3%;
			zoom:90%;
			padding-top:15px;
			padding-bottom:15px;
			
			background-color:#4D4D4D;
			 border: 15px solid black;
			 border-radius: 10px;
			-moz-border-radius: 10px;
			-webkit-border-radius: 10px;
			
			  -webkit-transition: all 0.5s ease-in-out;
			  -moz-transition: all 0.5s ease-in-out;
			  -o-transition: all 0.5s ease-in-out;
			  transition: all 0.5s ease-in-out;
}
			
		
	   
	   #leftScreen,#leftScreenContent, #middleScreen, #rightScreen,#rightScreenContent
			{
				 position:absolute;
				 height: 200px;
				 width: 200px;
				 margin:2px;
				 background-color:beige;
				 z-index:1;
				 
				
			}
			
		  
	   #leftPhoto, #rightPhoto
			{
				 position:relative;
				 float:left;
				 margin:10px;
				 height: 200px;
				 width: 300px;
				 background-color:black;
				 z-index:1;
				 
				
			
			}	
			
			
		 #middlePhoto
			{
				 position:relative;
				 float:left;
				 margin:10px;
				 height: 200px;
				 width: 300px;
				 background-color:grey;
				 z-index:4;
				 
				border: 10px ridge brown;
				border-radius: 10px;
				-moz-border-radius: 10px;
				-webkit-border-radius: 10px; 
			
			}	
			
			
			#middlePhoto:hover {zoom:250%;left:10%;top:-200%}
			
		#leftPhotoPointer
			{
				 position:relative;
				 float:left;
				 margin:10px;
				 height:200px;
				 width:75px;
				 background-color:transparent;
				 z-index:4;
				 
				
			}	

		 #rightPhotoPointer
			{
				 position:relative;
				 float:right;
				 margin:10px;
				 height:200px;
				 width:75px;
				 background-color:transparent;
				 z-index:4;
				 
				
			}			
		
		#leftPhotoContent {position:absolute;
						   height:100%;
						   width:100%;
						   -webkit-transform:rotateY(-45deg);
						   background-color:grey;
						   	 
							border: 10px ridge brown;
							border-radius: 10px;
							-moz-border-radius: 10px;
							-webkit-border-radius: 10px; 
							 }
		#rightPhotoContent {position:absolute;
						   height:100%;
						   width:100%;
						   -webkit-transform:rotateY(45deg);
						   	 background-color:grey;
							 
							border: 10px ridge brown;
							border-radius: 10px;
							-moz-border-radius: 10px;
							-webkit-border-radius: 10px; 
		}
			
		#leftPointer, #rightPointer {
									 position:absolute;
									 height: 200px;
									 width:75px;
								     background-color:transparent;
									 display:block;
									 z-index:2;
									 }
		#leftPointer {left:0%}
		#rightPointer {left:91%}
		
	
		
		#middleScreen, #leftScreenContent, #rightScreenContent {
				 border: 5px solid brown;
				 border-radius: 5px;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
				overflow:hidden;
				opacity:0.75;
				padding:5px;
				font-size:0.7em;
				}
				
				
	
			
			
		#leftScreen {left:12%;margin-left:1%; background-color:transparent} 
		#leftScreenContent {left:-25%;margin-left:1%;-webkit-transform:rotateY(-45deg);z-index:2;background-color:beige;} 
		#middleScreen {
						left:23%;margin-left:1%;z-index:4;top:-20%;
						zoom:200%;opacity:1.0;
						color:brown;
						overflow-y:scroll;
						-webkit-transform: scale(1.0,1.5);
						-moz-transform: scale(1.0,1.5);
						transform: scale(1.0,1.5);
						}
		#rightScreen {left:65%;margin-left:1%; background-color:transparent}
		#rightScreenContent {left:9%;margin-left:1%;-webkit-transform:rotateY(45deg);z-index:2;background-color:beige;}
	
		
		
		
		
		
		
		.playerBase {
						display:none;
						position:relative;
						float:left;
						height:230px;
						width:1100px;
						left:-78%;
						margin-top:10%;
						background-color:black;
						padding-left:40px;
						zoom:60%;
						z-index:4;

						-webkit-transform: scale(1.25,1);
						-moz-transform: scale(1.25,1)
						transform: scale(1.25,1);
						
						 border: 25px solid black;
						 border-radius: 15px;
						-moz-border-radius: 15px;
						-webkit-border-radius: 15px;
					}
		.playerBase2 {
						display:none;
						position:relative;
						float:left;
						height:50px;
						width:865px;
						left:-78%;
						padding:5px;
						background-color:#333333;
						zoom:70%;
						
						 border: 35px solid black;
						 border-radius: 15px;
						-moz-border-radius: 15px;
						-webkit-border-radius: 15px;
					}
		
	    #leftScreen:before {
					content: "";
					width: 0;
					height:0;
					position: absolute;
					left: -35%;
					top: 72px;
					margin-left: -20px;
					border-width: 20px 20px 20px 0px;
					border-style: solid;
					border-color: transparent white; 
					
				}
		
		#rightScreen:after {
					content: "";
					width: 0;
					height:0;
					position: absolute;
					left:134%;
					top: 72px;
					margin-left: -20px;
					border-width: 20px 0px 20px 20px;
					border-style: solid;
					border-color: transparent white;
					
				}
		
		#leftPhoto{left:-10%}
		#rightPhoto{left:-9%}
		#leftPhotoContent{left:20%}
		#middlePhoto{left:-7%}
		#rightPhotoContent{left:-3%}
		#leftPhotoPointer {left:-1%}
		#rightPhotoPointer {top:-91%;left:-4%}
		#leftPhoto:before {
					content: "";
					width: 0;
					height:0;
					position: absolute;
					left: 15%;
					top: 72px;
					margin-left: -20px;
					border-width: 20px 20px 20px 0px;
					border-style: solid;
					border-color: transparent white; 
					
				}
		
		#rightPhoto:after {
					content: "";
					width: 0;
					height:0;
					position: absolute;
					left:112%;
					top: 72px;
					margin-left: -20px;
					border-width: 20px 0px 20px 20px;
					border-style: solid;
					border-color: transparent white;
					
				}
		
	
*{ margin: 0; padding: 0;}
/*  Clouds and background into which clouds blend off scene */
body {
	/*To hide the horizontal scroller appearing during the animation*/
	overflow: hidden;
	background-color:white;
	height:300%;
	width:100%;
	font-family:"Comic Sans", Comic Sans MS, cursive;
	font-size:0.8em;
	
}


h1{margin-left:28%;width:50%;margin-top:1.5%;color:brown;font-size:3.3em}

#clouds{
    position:absolute;
	padding: 100px 0;
	z-index:4;
	top:-100%;

}

/* Cloud shape class and pseudo elements */
.cloud {
	width: 200px; height: 60px;
	background: #fff;
	border-radius: 200px;
	-moz-border-radius: 200px;
	-webkit-border-radius: 200px;
	z-index:3;
	position: relative; 
}

.cloud:before, .cloud:after {
	content: '';
	position: absolute; 
	background: #fff;
	width: 100px; height: 80px;
	position: absolute; top: -15px; left: 10px;
	
	border-radius: 100px;
	-moz-border-radius: 100px;
	-webkit-border-radius: 100px;
	
	-webkit-transform: rotate(30deg);
	transform: rotate(30deg);
	-moz-transform: rotate(30deg);
}

.cloud:after {
	width: 120px; height: 120px;
	top: -55px; left: auto; right: 15px;
}

.clkmess {font-style:italic;padding:10px}

/* Classes to animate individual clouds */
/* Opacity and speeds set for overlap */
.x1 {
	left: 125%;
    top:90px;
	-webkit-transform: scale(0.25);
	-moz-transform: scale(0.25);
	transform: scale(0.25);
    opacity:0.2;
	/*Cloud animation with speed */
	-webkit-animation: moveclouds 15s linear infinite;
	-moz-animation: moveclouds 15s linear infinite;
	-o-animation: moveclouds 15s linear infinite;
}

/*variable speed, opacity, and position of clouds for realistic effect*/
.x2 {
	left: 125%;
	top:40px;
	
	-webkit-transform: scale(0.3);
	-moz-transform: scale(0.3);
	transform: scale(0.3);
	opacity: 0.2; 

	-webkit-animation: moveclouds 25s linear infinite;
	-moz-animation: moveclouds 25s linear infinite;
	-o-animation: moveclouds 25s linear infinite;
}

.x3 {
	left: 125%; top: -15px;
	
	-webkit-transform: scale(0.4);
	-moz-transform: scale(0.4);
	transform: scale(0.4);
	opacity: 0.2; 
	
	-webkit-animation: moveclouds 17s linear infinite;
	-moz-animation: moveclouds 17s linear infinite;
	-o-animation: moveclouds 17s linear infinite;
}

.x4 {
	left: 125%; top: -70;
	
	-webkit-transform: scale(0.35);
	-moz-transform: scale(0.35);
	transform: scale(0.35);
	opacity: 0.2; 
	-webkit-animation: moveclouds 18s linear infinite;
	-moz-animation: moveclouds 18s linear infinite;
	-o-animation: moveclouds 18s linear infinite;
}

.x5 {
	left: 125%; top: -130px;
	
	-webkit-transform: scale(0.4);
	-moz-transform: scale(0.4);
	transform: scale(0.4);
	opacity: 0.2; 
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

 /* Cloud animations */
@-webkit-keyframes moveclouds {
	0% {left: 125%;}
	100% {left:-75%;}
}
@-moz-keyframes moveclouds {
	0% {left: 125%;}
	100% {left: -75%;}
}
@-o-keyframes moveclouds {
	0% {left: 125%;}
	100% {left: -75%;}
}
		

 /* Bird animations */
@-webkit-keyframes movebirds {
	0% {left:125%;opacity:0.0}
	14% {left:76%;opacity:0.0}
	15% {left:75%;opacity:1.0}
	84% {left:-17%;opacity:1.0}
	85% {left:-18%;opacity:0.0}
	100% {left:-35%;opacity:0.0}
}
@-moz-keyframes movebirds {
	0% {left:125%;opacity:0.0}
	14% {left:76%;opacity:0.0}
	15% {left:75%;opacity:1.0}
	84% {left:-17%;opacity:1.0}
	85% {left:-18%;opacity:0.0}
	100% {left:-35%;opacity:0.0}
}
@-o-keyframes movebirds {
	0% {left:125%;opacity:0.0}
	14% {left:76%;opacity:0.0}
	15% {left:75%;opacity:1.0}
	84% {left:-17%;opacity:1.0}
	85% {left:-18%;opacity:0.0}
	100% {left:-35%;opacity:0.0}
}
		
@-webkit-keyframes flaptop {
	13% {left:78%;opacity:0.0}
	16% {left:74%;opacity:1.0}
	22% {left:66%;opacity:0.0}
	29% {left:57%;opacity:1.0}
	36% {left:48%;opacity:0.0}
	43% {left:39%;opacity:1.0}
	50% {left:30%;opacity:0.0}
	57% {left:21%;opacity:1.0}
	64% {left:12%;opacity:0.0}
	71% {left:3%;opacity:1.0}
	78% {left:-6%;opacity:0.0}
	83% {left:-16%;opacity:1.0}
	87% {left:-20%;opacity:0.0}
}
@-moz-keyframes flaptop {
	13% {left:78%;opacity:0.0}
	16% {left:74%;opacity:1.0}
	22% {left:66%;opacity:0.0}
	29% {left:57%;opacity:1.0}
	36% {left:48%;opacity:0.0}
	43% {left:39%;opacity:1.0}
	50% {left:30%;opacity:0.0}
	57% {left:21%;opacity:1.0}
	64% {left:12%;opacity:0.0}
	71% {left:3%;opacity:1.0}
	78% {left:-6%;opacity:0.0}
	83% {left:-16%;opacity:1.0}
	87% {left:-20%;opacity:0.0}
}
@-o-keyframes flaptop {
	13% {left:78%;opacity:0.0}
	16% {left:74%;opacity:1.0}
	22% {left:66%;opacity:0.0}
	29% {left:57%;opacity:1.0}
	36% {left:48%;opacity:0.0}
	43% {left:39%;opacity:1.0}
	50% {left:30%;opacity:0.0}
	57% {left:21%;opacity:1.0}
	64% {left:12%;opacity:0.0}
	71% {left:3%;opacity:1.0}
	78% {left:-6%;opacity:0.0}
	83% {left:-16%;opacity:1.0}
	87% {left:-20%;opacity:0.0}
}	

@-webkit-keyframes flapbottom {
	13% {left:78%;opacity:0.0}
	16% {left:74%;opacity:0.0}
	22% {left:66%;opacity:1.0}
	29% {left:57%;opacity:0.0}
	36% {left:48%;opacity:1.0}
	43% {left:39%;opacity:0.0}
	50% {left:30%;opacity:1.0}
	57% {left:21%;opacity:0.0}
	64% {left:12%;opacity:1.0}
	71% {left:3%;opacity:0.0}
	78% {left:-6%;opacity:1.0}
	83% {left:-16%;opacity:0.0}
	87% {left:-20%;opacity:0.0}
}
@-moz-keyframes flapbottom {
	13% {left:78%;opacity:0.0}
	16% {left:74%;opacity:0.0}
	22% {left:66%;opacity:1.0}
	29% {left:57%;opacity:0.0}
	36% {left:48%;opacity:1.0}
	43% {left:39%;opacity:0.0}
	50% {left:30%;opacity:1.0}
	57% {left:21%;opacity:0.0}
	64% {left:12%;opacity:1.0}
	71% {left:3%;opacity:0.0}
	78% {left:-6%;opacity:1.0}
	83% {left:-16%;opacity:0.0}
	87% {left:-20%;opacity:0.0}
}
@-o-keyframes flapbottom {
	13% {left:78%;opacity:0.0}
	16% {left:74%;opacity:0.0}
	22% {left:66%;opacity:1.0}
	29% {left:57%;opacity:0.0}
	36% {left:48%;opacity:1.0}
	43% {left:39%;opacity:0.0}
	50% {left:30%;opacity:1.0}
	57% {left:21%;opacity:0.0}
	64% {left:12%;opacity:1.0}
	71% {left:3%;opacity:0.0}
	78% {left:-6%;opacity:1.0}
	83% {left:-16%;opacity:0.0}
	87% {left:-20%;opacity:0.0}
}	
		


		
	
	
</style>


<body onload="JQFunctions()">
<h1>Discovering Good Living In Las Vegas</h1>
<div class="topWindow">
	
	<div class="scene"><span class="shade"></span>
		
			  <div class="birdFly1">
				<img src="<?php echo $birdFly1; ?>" alt="FlyingBird1" height="73" width="77">
			  </div>
			  <div class="birdFly2">
				<img src="<?php echo $birdFly2; ?>" alt="FlyingBird2" height="73" width="77">
			  </div>
			  <div class="secondbirdFly1">
				<img src="<?php echo $birdFly1; ?>" alt="FlyingBird1" height="73" width="77">
			  </div>
			  <div class="secondbirdFly2">
				<img src="<?php echo $birdFly2; ?>" alt="FlyingBird2" height="73" width="77">
			  </div>
			  <div class="thirdbirdFly1">
				<img src="<?php echo $birdFly1; ?>" alt="FlyingBird1" height="73" width="77">
			  </div>
			  <div class="thirdbirdFly2">
				<img src="<?php echo $birdFly2; ?>" alt="FlyingBird2" height="73" width="77">
			  </div>
			  <div class="sun">
				
					<div class="street">
						<div class="kidsPlay"></div>
					</div>
				
			  </div>
			  <div class="moon">
			  </div>
			  <div class="kidsPlay">
				<img src="<?php echo $kidsPlaying; ?>" alt="Kids Playing" height="225" width="225">
			  </div>
              <div id="cloudContainer">
				  <div id="clouds">
					<div class="cloud x1"></div>
					<div class="cloud x2"></div>
					<div class="cloud x3"></div>
					<div class="cloud x4"></div>
					<div class="cloud x5"></div>
				  </div>
			  </div>
	
			  
		
	</div>
	<div class="container">
			<div class="viewer">
				<div class="displayer">
					<div id="leftPointer"></div>
					<div id="leftScreen"><div id="leftScreenContent"></div></div>
					<div id="middleScreen"></div>
					<div id="rightScreen"><div id="rightScreenContent"></div></div>
					<div id="rightPointer"></div>
				</div>
				
			</div>
			<div class="playerBase2"></div>
			<div class="playerBase">
					<div id="leftPhotoPointer"></div>
					<div id="leftPhoto"><div id="leftPhotoContent"></div></div>
					<div id="middlePhoto"></div>
					<div id="rightPhoto"><div id="rightPhotoContent"></div></div>
					<div id="rightPhotoPointer"></div>
				
			</div>	
	</div>
	
</div>
	


</body>


</html>