<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/*
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($app='desktop')
	{
		if($this->input->get('app'))
			 $app = $this->input->get('app');
			
		//echo $app;	
	    
		//$this->load->view('welcome_message');
	    require_once(APPPATH.'controllers/portfolio.php'); //include controller
           $aObj = new portfolio();  //create object 
           $aObj->index($app); //call function  
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */