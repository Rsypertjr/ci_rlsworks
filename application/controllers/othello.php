<?php
class Othello extends CI_Controller {
    
    
	public function __construct()
	{
	
	        parent::__construct();  
            //  LOAD HELPERS AND LIBRARIES
            $this->load->helper('url'); 
            $this->load->helper('html');
            $this->load->helper('file');
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->helper('cookie');
			$this->load->helper('download');
			$this->load->library('email');
			
		
			//Database Configurations and Model Initialization
			$config['hostname'] = "localhost";
			$config['username'] = "rlsworks_richard";
			$config['password'] = "syp3rt";
			$config['database'] = "rlsworks_photolinks_db";
			
			//$config['hostname'] = $server;
			//$config['username'] = $username;
			//$config['password'] = $password;
			//$config['database'] = $db;
			
			$config['dbdriver'] = "mysql";
			$config['dbprefix'] = "";
			$config['pconnect'] = FALSE;
			$config['db_debug'] = TRUE;
			$config['cache_on'] = FALSE;
			$config['cachedir'] = "";
			$config['char_set'] = "utf8";
			$config['dbcollat'] = "utf8_general_ci";
			
			
			$config2['hostname'] = "localhost";
			$config2['username'] = "rlsworks_richard";
			$config2['password'] = "syp3rt";
			$config2['database'] = "rlsworks_app_images";
			$config2['dbdriver'] = "mysql";
			$config2['dbprefix'] = "";
			$config2['pconnect'] = FALSE;
			$config2['db_debug'] = TRUE;
			$config2['cache_on'] = FALSE;
			$config2['cachedir'] = "";
			$config2['char_set'] = "utf8";
			$config2['dbcollat'] = "utf8_general_ci";

			$this->load->model('appimages_model','',$config2);
			$this->appimages_model->initialize();
			
		//	$this->load->model('photolinks_model','',$config);
		//	$this->photolinks_model->initialize();
			
			
			/*****************************************************************************************************/
			/* BASE URL Dependent variables that need explicit change for Production.  Base_url in file config/config.php needs to be   */
			/* changed to Production Host  */
		    $this->data['mobile'] = base_url('index.php?app=mobile');  // change from https to http
			$this->mdata['mobileIndex2'] = base_url('index2');  // ditto
			  // $this->data['indexTwo'] =  base_url('index2');
            $this->data['indexTwo'] =  base_url('index.php?app=index2');
            $this->othello_data['indexTwo'] =  base_url('index.php?app=index2');
			$this->data['homePage'] = base_url('');
			$this->othello_data['homePage'] = base_url('');
			$this->data['prodSite'] = C9_PRODUCTION_BASE_URL_SSL;
			$this->data['devSite'] = C9_DEVELOPMENT_BASE_URL_SSL;
			//$this->data['prodSite'] = base_url('../ci');
			$this->data['newOrominerXML'] = base_url('xml/oro_xml4.xml');
			$this->data['orominerHistoXML'] = base_url('xml/oro_xml.xml');
			
			//  XML File for NEW OROMINER and Orominer with Histological Info 
			// Link to generate database for MiniMotif App 
		    $this->data['getMotifData'] = base_url("getMotifs/getMotifs.php?");
			/******************************************************************************************************/
			
			
			/********************Othello Game Header variables  ****************/
            $this->othello_data["boardRows"] = array( "........", "........", "........", "...XO...", "...OX...", "........", "........", "........");

            $this->othello_data["twoDimBoard"] = array();
            $this->othello_data["winner"] = null;
            $this->othello_data["turnHash"] = "";
            //$gameId = $_POST["gameId"];
            $this->othello_data["board"] = "";
            $this->othello_data["round"] = 0;
            $this->othello_data["turn"] = null;
            $this->othello_data["randTurn"] = null;
            $this->othello_data["playerKeyShaMap"] = array();
            
            $this->othello_data["doc"] = new DOMDocument();
            $this->othello_data["doc2"] = new DOMDocument();
            $this->othello_data["doc"]->validateOnParse = true;
          
          
            // Initialize Game Tables
            $this->othello_data["myinitboard"] = $this->load->view("othello/myinitboard","",TRUE);
            $this->othello_data["doc2"]->loadHTML($this->othello_data["myinitboard"]);
         
            $this->othello_data["myBoard"] = ""; 
            // $this->othello_data["myBoard"] = $this->load->view("othello/myboard","",TRUE);
            
            //Try loading from Database
            $this->othello_data["myBoard"] = $this->selectTable('myBoard');
            //$GLOBALS['myBoard'] = $this->othello_data["myBoard"];
            $GLOBALS['myBoard'] = $this->othello_data["myinitboard"];
            $this->othello_data["doc"]->loadHTML($this->othello_data["myinitboard"]);
            $this->othello_data["x"] = new DOMXPath($this->othello_data["doc"]);
            
            $this->othello_data["cells"] = array(); 
            $this->othello_data["cellArray"] = array();
            for($i=1;$i<=8;$i++)
                {
                        $rows = $this->othello_data["doc"]->getElementById("row".$i);
                        $this->othello_data["cells"][$i-1] = $rows->getElementsByTagName("div");
                      $j= 0;
                     foreach($this->othello_data["cells"][$i-1] as $cell)
                            {
                                $this->othello_data["cellArray"][$i-1][$j] = $cell->nodeValue;
                                $j++;
                            }                  
                }
                
            $this->othello_data["rows"] = array($this->othello_data["x"]->query("//*[@id='row1']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row2']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row3']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row4']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row5']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row6']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row7']")->item(0),
                                                             $this->othello_data["x"]->query("//*[@id='row8']")->item(0)
                                                         );

            $this->othello_data["flipSpaces"] = array();

            $this->othello_data["possibleMoves"] = array();
            //$this->othello_data["emptySpaces"] = array();
            $this->othello_data["randMove"] = array();
            $this->othello_data["player"] = array();
            $this->othello_data["player"][0][0] = 2;
            $this->othello_data["player"][0][1] = 3;
            $this->othello_data["player"][0][2] = 'O';
            $this->othello_data["player"][1][0] = 4;
            $this->othello_data["player"][1][1] = 2;
            $this->othello_data["player"][1][2] = 'O';
            $this->othello_data["message"] = "";
                
            $this->othello_data["count"]=0;
            $this->othello_data["symbol"] = 'O';
            $this->othello_data["gamebegin"] = 0;
        
            $this->othello_data["playr1Key"] = "richardlsypertjr";
            $this->othello_data["playr2Key"] = "autoplayer";
            $this->othello_data["twoDimBoard"] = $this->othello_data["cellArray"];
          
            // Images
           	$this->othello_data['ximg']= base_url('images/ximg.jpg');
			$this->othello_data['oimg']= base_url('images/oimg.jpg');
           
           $this->othello_data['css'] = base_url('css/othello2.css');
		   $this->othello_data['othelloAjaxMode'] = 'no';
		   $this->othello_data['othelloIndexTwo'] = base_url('index.php?app=index2');
           
           // Clear Boards in Othello Game
           $this->printBoard($this->othello_data["twoDimBoard"],$this->othello_data["cells"],$this->othello_data["doc"]);
           
            /*Generate JSON
                  $jsonString = '{"winner":'.$winner.
                        ',"turnHash":'.$turnHash.
                        ',"gameId":'.$gameId.
                        ',"board": ["'.implode($twoDimBoard[0]).
                        '","'.implode($twoDimBoard[1]).
                        '","'.implode($twoDimBoard[2]).
                        '","'.implode($twoDimBoard[3]).
                        '","'.implode($twoDimBoard[4]).
                        '","'.implode($twoDimBoard[5]).
                        '","'.implode($twoDimBoard[6]).
                        '","'.implode($twoDimBoard[7]).
                        '"],"round":'.$round.
                        ',"turn":'.$turn.
                        ',"playerKeyShaMap": {'.
                        $playr1Key.":".$turn.','.
                        $playr2Key.":".$randTurn.'}}';
           
           
           
		   /**********************  End Othello Game Header variables  */
		   
		   
		
			
			/******************* DESKTOP APP HEADER VARIALBES  *************************************************/
			/* CSS File LINKS   */
	        $this->data['css'] = base_url('css/othello2.css');	
			$this->data['livingInLVCSS'] = base_url('css/livingInLVCSS.css');
			$this->data['frontCSS'] = base_url('css/frontCSS.css');
			$this->data['miniMotifCSS'] = base_url('css/miniMotifCSS.css');
			$this->mdata['miniMotifCSS'] = base_url('css/miniMotifCSS.css');
			$this->data['newOrominerCSS'] = base_url('css/newOrominerCSS.css');
			$this->data['orominerHistoCSS'] = base_url('css/orominerHistoCSS.css');
			$this->data['othelloCSS'] = base_url('css/othelloCSS.css');
			$this->data['codeDevCSS'] = base_url('css/codeDevCSS.css');
			$this->data['techWriterCSS'] = base_url('css/techWriterCSS.css');
			$this->data['resumesCSS'] = base_url('css/resumesCSS.css');
			$this->data['headerCSS'] = base_url('css/headerCSS.css');
			$this->data['showPDFCSS'] = base_url('css/showPDFCSS.css');
			$this->data['footerCSS'] = base_url('css/footerCSS.css');
			$this->data['emailFormCSS'] = base_url('css/emailFormCSS.css');
			$this->data['webTechCSS'] = base_url('css/webTechCSS.css');
			$this->data['mobileSwitchCSS'] = base_url('css/mobileSwitchCSS.css');
            $this->data['jqueryloc']= base_url('assets/js/jquery-1.10.2.js');
            $this->data['jqfile']= base_url('assets/js/global.js');
            $this->data['jqUiCSS'] = base_url('css/excite-bike/jquery-ui-1.10.4.custom.css');
			$this->data['jqJS'] = base_url('js/jquery-1.10.2.js');
			$this->data['jqUiJS'] = base_url('js/jquery-ui-1.10.4.custom.js');
		
		
			// For Header images Storage for desktop
			$this->data['headerImages'] = array();
			
			$this->data['imgfile2']= base_url('images/engImg.jpg');
		
		    //  Initial App Settings
			$this->data['whichPage'] = "";
			$this->data['appType'] = "desktop";
			$this->data['currentScrollPos'] = 0;
			$this->data['appType2'] = "default";
			$this->data["hfSwitch"] = 'off';
			/************************************ End Of DESKTOP APP Header Variables *******************************/	
			
		
		
	        /***********************************  MOBILE APP HEADER VARIABLES   **************************************/	
			//links for Mobile site
			$this->mdata['jqMobileCSS1'] = base_url('css/jquery.mobile.icons.min.css');	
			$this->mdata['jqMobileCSS2'] = base_url('css/mobile-bike.min.css');
			// Send to Desktop as well
			$this->data['jqMobileCSS1'] = base_url('css/jquery.mobile.icons.min.css');	
			$this->data['jqMobileCSS2'] = base_url('css/mobile-bike.min.css');
			$this->mdata['miniMotif_mobile']= base_url('index.php?app=mobile&appl=miniMotif&apType=mobile');
			$this->mdata['codeDevTech_mobile'] = base_url('index.php?app=mobile&appl=codetech&apType=mobile');
			$this->mdata['othello_mobile'] = base_url('index.php?app=mobile&appl=othello&apType=mobile');
			$this->mdata['orominer_mobile'] = base_url('index.php?app=mobile&appl=orominer&apType=mobile');
			$this->mdata['email_page'] = base_url('index.php?app=mobile&appl=email1&apType=mobile');
			$this->mdata['oroHist_mobile'] = base_url('index.php?app=mobile&appl=oroHist&apType=mobile');
			$this->mdata['livingInVegas_mobile'] = base_url('index.php?app=mobile&appl=livingInVegas&apType=mobile');
			$this->mdata['resume_mobile'] = base_url('index.php?app=mobile&appl=resume&apType=mobile');
			$this->mdata["hfSwitch"] = 'off';
			
		
		    // mobile app css file
			$this->mdata['showMobilePDFCSS'] = base_url('css/showPDFCSS.css');  
			$this->mdata['codeDevMobileCSS'] = base_url('css/codeDevMobileCSS.css');
			$this->mdata['othelloMobileCSS'] = base_url('css/othelloMobileCSS.css');
			$this->mdata['mobileCSS'] = base_url('css/mobileCSS.css');
			/********************************  End Of MOBILE APP HEADER Variables  ******************************************/
     
    }
    

    
    
function index($app='desktop')
{
		 /** DESKTOP APPLICATION HEADER AND FOOTER SETUP **/
		  if($app == 'desktop')
			{
				// $this->data['whenMobileCSS'] = base_url('css/whenDesktopCSS.css');  //keep desktop header/footer
				// Send to header to load correct CSS files
			         
						$this->data['whichPage'] = "front";
					
						//Get header image links from model to global array variable
						$this->data['headerImages'] =  $this->appimages_model->get_desktop_images();
					
				        //print_r($this->llv_data['headerImages']);
						
						//Load header, Page, and Footer
						$this->load->view('header/pageHeader',$this->data);
						$this->load->view('rlsworks/front');
						$this->load->view('rlsworks/frontFoot');
			}
		/** MOBILE APPLICATION SELECTIONS  ***/	
		 else if($app == 'mobile')
			{
			//	$this->data['whenMobileCSS'] = base_url('css/whenMobileCSS.css');  //eliminate desktop header/footer on mobile
				$selectedMode='';
				$mobileAppmode = $this->input->get('appl');
			    $moBileAppType = $this->input->get('apType');
			    
				
				//Get header image links from model to global array variable
				$this->mdata['headerImages'] = $this->appimages_model->get_desktop_images();
			    $this->data['headerImages'] = $this->appimages_model->get_desktop_images();
																						
				if($mobileAppmode == null)
					{
						$this->load->view('mobile/mobileHeader.php', $this->mdata);
						$this->load->view('mobile/frontPageMobile.php',$this->mdata);
						$this->load->view('mobile/mobileFooter.php');
					}
				
			    if($mobileAppmode != null)
					$selectedMode = $mobileAppmode;
				
				
				
				if($selectedMode == 'codetech')    // for Mobile App
					{
						$this->index2('codetech',$moBileAppType);
					}
						
				else if($selectedMode == 'othello')    // for Mobile App
					{
						$this->index2('othello',$moBileAppType);
					}

					
				else if($selectedMode == 'miniMotif')    // for Mobile App
					{
					  $this->index2('miniMotif',$moBileAppType);
					}	
					
				else if($selectedMode == 'orominer')    // for Mobile App
					{
					  $this->index2('orominer',$moBileAppType);
					}	
					
				else if($selectedMode == 'oroHist')    // for Mobile App
					{
					  $this->index2('oroHist',$moBileAppType);
					}	
				
				else if($selectedMode == 'livingInVegas')    // for Mobile App
					{
					  $this->index2('livingInVegas',$moBileAppType);
					}	
				
				else if($selectedMode == 'resume')    // for Mobile App
					{
					  $this->index2('resume',$moBileAppType);
					}	
					
				else if($selectedMode == 'email1')    // for Mobile App
					{
					  $this->index2('email1',$moBileAppType);
					}	
				else;
						
					
			}
				
		/** DESKTOP APPLICATION SELECTIONS **/		
		else if($app == 'index2')
			{
				$this->index2();
			}
		else if($app == 'processEmail')
			{
				$this->processEmail();
			}
		else if($app == 'playbegin')
			{
				$this->playbegin();
			}	
		else if($app == 'othello')    // for Mobile App
			{
				$this->index2('othello');
			}
		else if($app == 'livingInVegas')    // for Mobile App
			{
				$this->index2('livingInVegas');
			}	
		else if($app == 'resume')    // for Mobile App
			{
				$this->index2('resume');
			}
		else if($app == 'technicalWriting')    // for Mobile App
			{
				$this->index2('technicalWriting');
			}
		else if($app == 'orominer')    // for Mobile App
			{
				$this->index2('orominer');
			}	
		else if($app == 'oroHist')    // for Mobile App
			{
				$this->index2('oroHist');
			}
		else if($app == 'miniMotif')    // for Mobile App
			{
				$this->index2('miniMotif');
			}	
		else if($app == 'email1')    // for Mobile App
			{
				$this->index2('email1');
			}	
		else;
			
}


/********************* PROCESSOR FOR DESKTOP AND MOBILE APP SELECTIONS **/
private function index2($mobileitem = null, $applicType = null)
        {  // $mobileitem value from index function when application type is 'mobile'
		
		     // option selected by posting from Header to index2 function by (itemchoice,itemchoice2) input fields in footer
		     // when application type is 'desktop'. $desktopItem1 and $desktopItem2 capture both header and footer view selections
		     
		     $desktopItem1 = $this->input->post('itemchoice');
			 $desktopItem2 = $this->input->post('itemchoice2');
             $deskorMobile = $this->input->post('appType');
           
			 $itemchoice = null;
			 
		     if($desktopItem1 !="none" && $desktopItem2=='none')
				$mode = $itemchoice = $this->input->post('itemchoice');
				
		     if($desktopItem2!='none' && $desktopItem1=='none')
				$mode = $itemchoice2 = $this->input->post('itemchoice2');
		 
		     $othelloMode = '';

			 // Posting from Othello Game for play options
		     $submit = $this->input->post('submit');
			 if($submit == "Press to Play" || $submit == "Press to Continue" || $submit == "Submit This Move")
				 $mode = 'playbegin';
		
		
			 // Application types posted to controller
			 $otheLlomodeType1 = $this->input->post('applType');
			 $othellomodeType2 = $this->input->post('applicationType');
			 if($otheLlomodeType1 == null)
				$otheLlomodeType1 = 'desktop';
			 if($othellomodeType2 != null)
				$otheLlomodeType1 = $othellomodeType2;
			 if($applicType != null)
				$otheLlomodeType1 = $applicType;
		
		    
			
			 //check type of app base on url segment instead of form posting
			 $appCheck = $this->uri->segment(1);   // Index2 option or itemchosen argument value
			 $appCheck2 = $this->uri->segment(2);   // Application Type
		
			 if($mobileitem != null && $itemchoice == null)  // Let posting from header override other option values
				{
					$mode = $mobileitem;
					$appType = $appCheck2;
				}
			 
			 
			 if($appCheck == 'index2' && $appCheck2 != null)  // When posted to base controller method and $appCheck is index2 method
				$mode = $appCheck2;
		
			// When Reset Modes are chosen from Othello Game
	    	 if($this->input->post('othelloChoice'))
				{
					$othelloMode = $this->input->post('othelloChoice');	
					$mode = $othelloMode;
			    } 
				
            // Initialize Othello Player Input Board
             $this->othello_data["display1"] = "none";
             
             $this->othello_data["display2"] = "block";
             $this->othello_data["display3"] = "block";
             $this->othello_data["display4"] = "none";
			
			
            if($appCheck2 == 'mobile' || $otheLlomodeType1 == 'mobile' || $deskorMobile == 'mobile')
				{
					$this->data['appType'] = 'mobile';
					$this->othello_data['appType'] = 'mobile';
                    $this->mdata['appType'] = 'mobile';
					$appType = 'mobile';
					
				}
			else
				{
					$this->data['appType'] = 'desktop';
					$this->othello_data['appType'] = 'desktop';
                    $this->mdata['appType'] = 'desktop';
					$appType = 'desktop';
				}
			 

			
			 $this->data["itemchosen"] = $mobileitem;
			 
			 // Select CSS file for mobile app header
			 if($mode == 'othello')
				$this->mdata['mobileCSS'] = $this->mdata['othelloMobileCSS'];
			 else if($mode == 'whitePaper' || $mode == 'engSpec' || $mode == 'graingerABCDE' || $mode == 'graingerCDE' || $mode == 'mecProductManual')
				$this->mdata['mobileCSS'] = $this->mdata['showMobilePDFCSS'];
			 else if($mode == 'codetech')
				$this->mdata['mobileCSS'] = $this->mdata['codeDevMobileCSS'];
			 else if($mode == 'miniMotif')
				$this->mdata['mobileCSS'] = $this->data['miniMotifCSS'];
			 else if($mode == 'resume')
				$this->mdata['mobileCSS'] = $this->data['resumesCSS'];
				
	     
			if( $appType == 'desktop')
			    {
			    	$this->data["hfSwitch"] = $this->mdata["hfSwitch"] = 'off';
			    	//Get header image links from model to global array variable
					$this->data['headerImages'] = $this->appimages_model->get_desktop_images();
					//print_r($this->data['headerImages']);
			    }
			else if($appType == 'mobile')
				$this->data["hfSwitch"] = $this->mdata["hfSwitch"] = 'on';
			
		
			 if(($mode != 'codetech') && ($submit === 'Press to Play' || $submit === 'Press to Continue' || $submit === 'Submit This Move'))
		 		$mode = 'playbegin';

			
			if($this->input->post('isAjax'))
				$this->othello_data['othelloAjaxMode'] = $this->input->post('isAjax');
					
			
			
			// Suppress header for mobile Othello Game
			if($mode == 'playbegin' && $otheLlomodeType1 == 'mobile')
				$this->data["hfSwitch"] = $this->mdata["hfSwitch"] = 'on';
				
			
		
   
			// Section for Selection of app, app mode, or document 
             if($mode == 'othello' || $mode == 'othelloAjax' || $mode == 'resetGame')
				{      
			            
			            
			            
			            
			            
			            $isAjax = '';	
			            $ajaxPosted = $this->input->post('othelloAjaxMode'); 
			            $justBoard = 'no';
						if($mode == 'othelloAjax') 
							  {
								$this->othello_data['othelloAjaxMode'] = 'yes';
								$isAjax = 'yes';  
							  }
						else if($mode == 'othello')
						  {
							$this->othello_data['othelloAjaxMode'] = 'no';
							$isAjax = 'no';
							
							// Initialize Game Tables in Database
				            //$thebody = $this->othello_data["doc2"]->getElementById('gboard');
				            //$theBodyXML = $this->othello_data["doc2"]->saveXML($thebody);
				            //file_put_contents("application/views/othello/myboard.php",$theBodyXML);
				            $theBody = 	file_get_contents("application/views/othello/myinitboard.php");
				            $this->console_log($theBody);
				            $this->updateInitBoard($theBody);
				            $this->updateMyBoard($theBody);
				            $this->updateTheBody2($theBody);
							
					  	  }
					  	if($mode == 'resetGame')
					  	    {
					  	        if($ajaxPosted == 'yes'){
					  	        	$justBoard = 'yes';
					  	        	$this->othello_data['othelloAjaxMode'] = 'yes';
					  	        }
					  	        else if($ajaxPosted == 'no'){
					  	        	$justBoard = 'no';
					  	        	$this->othello_data['othelloAjaxMode'] = 'no';
					  	        }
					  	    } 	 
					  	    
					  	if($appType == 'mobile')
					  	 	$isAjax = 'yes';
					  	 	
						 $this->othello_data["message"] ="<span id='prbutton' style='color:purple'>**Press button to Play!**<br/>Your Symbol is ".$this->othello_data["symbol"]."</span>"; 
							// Display Board
						 $this->othello_data["flag"] = 'no';  
					
	              	     // Send to header to load correct CSS files
						 $this->data['whichPage'] = "othello";
						 //Load header, Page, and Footer
			            
						 if($isAjax == 'no')
							$this->othello_data['othelloAjaxMode'] = 'no';
						 else if($isAjax == 'yes' )
							$this->othello_data['othelloAjaxMode'] = 'yes'; 
						     	
			            //echo $justBoard;    	
						//Load header, Page, and Footer\
						if($justBoard == 'no')  //for Ajax mode only update board
							$this->load->view('header/pageHeader',$this->data);
						$this->load->view('othello/header',$this->othello_data);
						$this->load->view('othello/myinitboard');
						$this->load->view('othello/footer'); 
						if($justBoard == 'no')
							$this->load->view('rlsworks/frontFoot'); 
				}
				
				
		     else if($mode == 'orominer')
				{
					  // Send to header to load correct CSS files
				      $this->data['whichPage'] = "newOrominer";
					  
					  //Load header, Page, and Footer
					  $this->load->view('header/pageHeader',$this->data);
					  
					  $this->load->view('orominer/newOrominer.php'); 
					  $this->load->view('rlsworks/frontFoot');
				}
             
			
			 else if($mode == 'miniMotif')
				{
				
					$this->data['whichPage'] = "miniMotif";
					$this->mdata['whichPage'] = "miniMotif";
					
					 // Send to header to load correct CSS files
					
					 $this->data['dbGet'] = base_url('fastaToMySQL.php');
					 //echo  $this->data['dbGet'];
					 $this->data['fileGet'] = base_url('FASTA-TEST.txt');
					 //Load header, Page, and Footer
					 $this->load->view('header/pageHeader',$this->data);
					 $this->load->view('miniMotif/motifInput.php'); 
					 $this->load->view('rlsworks/frontFoot');
					
				}
     
             else if($mode == 'oroHist')
				{
					 // Send to header to load correct CSS files
				     $this->data['whichPage'] = "orominerHisto";
					 //Load header, Page, and Footer
					 $this->load->view('header/pageHeader.php',$this->data);
					 $this->load->view('orominer/orominerHisto.php'); 
					 $this->load->view('rlsworks/frontFoot.php');
               }
            
			
			 else if($mode == 'technicalWriting')
				{
					 // Send to header to load correct CSS files
				     $this->data['whichPage'] = "techWriter";
			
					 //Load header, Page, and Footer
					 $this->load->view('header/pageHeader',$this->data);
					 $this->load->view('techWriting/showTW.php'); 
					 $this->load->view('rlsworks/frontFoot');
               }
			   
		
			 else if($mode == 'webTech')
				{
					 // Send to header to load correct CSS files
				     $this->data['whichPage'] = "webTech";
					 //Load header, Page, and Footer
					 $this->load->view('header/pageHeader',$this->data);
					 $this->load->view('webTech/webTech.php'); 
					 $this->load->view('rlsworks/frontFoot');
               }
		
			else if($mode == 'frontPage')
				{
					 if($otheLlomodeType1 == 'desktop')
						$this->index('desktop');
					 else if($otheLlomodeType1 == 'mobile')
						$this->index('mobile');
					 else
						echo 'Application Type Error!';
               }	
				
				
			else if($mode == 'resume' || $mode == 'EngExp')
				{
					  // Send to header to load correct CSS files
				     $this->data['whichPage'] = "resumes";
					 $this->data['whichMode'] = 'showButtons';
                     $this->mdata['whichMode'] = 'noShowButtons';
					 //Load header, Page, and Footer
				  if($mobileitem == null) // option by posting
					{
					 $this->load->view('header/pageHeader',$this->data);
					 $this->load->view('resumes/RLSresume.php');
					 $this->load->view('rlsworks/frontFoot');
					}
				  else if($mobileitem != null)  // option by uri segment
					{
					 $this->load->view('mobile/mobileHeader.php',$this->mdata);
					 $this->load->view('resumes/RLSresume.php');
					 $this->load->view('mobile/mobileFooter.php');
					}
			 
               }	
			   
			else if($mode == 'codetech')
				{
					 if($appType == 'desktop') // option by posting
						{
						 // Send to header to load correct CSS files
						 $this->data['whichPage'] = "codeDev";
						 //Load header, Page, and Footer
						 $this->load->view('header/pageHeader',$this->data);
						 $this->load->view('techWriting/codeTW.php');
						 $this->load->view('rlsworks/frontFoot');
						}
					 else if($appType == 'mobile')  // option by uri segment
						{    
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('techWriting/codeTW.php');
							 $this->load->view('mobile/mobileFooter.php');
						}
						
               }	
			   
		    else if($mode == 'viewMobileResume')
						{
							 $this->load->view('resumes/showMobileResume.php');
						}
						
			else if($mode == 'viewPDFResume')
				{
			
					 
					 $this->data['pdfFilePath'] = $this->mdata['pdfFilePath']=base_url("pdf/RichardSypertJrResumePDF.pdf");
					 if($mobileitem == null) 
						{
							$this->data['whichPage'] = "viewPDFResume";
							$this->data['pdfFileName'] = "RichardSypertJrResumePDF.pdf";
							$this->load->view('header/pageHeader',$this->data);
							$this->load->view('showPDF/showAPDFDoc.php');
							$this->load->view('rlsworks/frontFoot');
						}
					 else if($mobileitem != null)
						{
							 $this->mdata['pdfFileName'] = "RichardSypertJrResumePDF.pdf";
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('showPDF/showAMobilePDFDoc.php');
							 $this->load->view('mobile/mobileFooter.php');
						}	 
					 
					 
					 
					
                }	
				
				
			else if($mode == 'downloadPDF')
				{
					$data = file_get_contents('pdf/RichardSypertJrResumePDF.pdf');
					$name = 'RichardSypertJrResumePDF.pdf';
					force_download($name, $data);
                }	
			      
			   
			else if($mode == 'email1' || $mode == 'email2')
				{
					 $this->data['emailType'] = $mode;
					 $mode=null;
					  //Load header, Page, and Footer
					 $this->data['whichPage'] = "emailForm"; 
					 $this->load->view('header/pageHeader',$this->data);
					 $this->load->view('email/emailForm.php');
					 $this->load->view('rlsworks/frontFoot');
				}
				
				
			else if($mode == 'whitePaper')
				{ 
					  //Load header, Page, and Footer
					 
					 if($mobileitem == null) 
						{
							$this->data['whichPage'] = "whitePaper";
							$this->tw_data['pdfFilePath'] = base_url("pdf/whitePaper.pdf");
							$this->tw_data['pdfFileName'] = "whitePaper.pdf";
							$this->load->view('header/pageHeader',$this->data);
							$this->load->view('showPDF/showAPDFDoc.php',$this->tw_data);
							$this->load->view('rlsworks/frontFoot');
						}
					 else if($mobileitem != null)
						{
							 $this->mdata['pdfFilePath'] = base_url("pdf/whitePaper.pdf");
						     $this->mdata['pdfFileName'] = "whitePaper.pdf";
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('showPDF/showAMobilePDFDoc.php');
							 $this->load->view('mobile/mobileFooter.php');
						}
					
				}	
			else if($mode == 'engSpec')
				{
					if($mobileitem == null) 
						{
						  //Load header, Page, and Footer
						 $this->tw_data['pdfFilePath'] = base_url("pdf/engSpec.pdf");
						 $this->tw_data['pdfFileName'] = "engSpec.pdf";
						 $this->data['whichPage'] = "engSpec";
						 $this->load->view('header/pageHeader',$this->data);
						 $this->load->view('showPDF/showAPDFDoc.php',$this->tw_data);
						 $this->load->view('rlsworks/frontFoot');
						}
						
				    else if($mobileitem != null)
						{
							 $this->mdata['pdfFilePath'] = base_url("pdf/engSpec.pdf");
						     $this->mdata['pdfFileName'] = "engSpec.pdf"; 
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('showPDF/showAMobilePDFDoc.php');
							 $this->load->view('mobile/mobileFooter.php');
						}
				}	
			else if($mode == 'GraingerABCDE')
				{
					
					if($mobileitem == null) 
						{
							  //Load header, Page, and Footer
							 $this->tw_data['pdfFilePath'] = base_url("pdf/graingerABCDE.pdf"); 
							 $this->tw_data['pdfFileName'] = "graingerABCDE.pdf";
							 $this->data['whichPage'] = "GraingerABCDE";
							 $this->load->view('header/pageHeader',$this->data);
							 $this->load->view('showPDF/showAPDFDoc.php',$this->tw_data);
							 $this->load->view('rlsworks/frontFoot');
						}
					else if($mobileitem != null)
						{
							 $this->mdata['pdfFilePath'] = base_url("pdf/graingerABCDE.pdf");  
							 $this->mdata['pdfFileName'] = "graingerABCDE.pdf";
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('showPDF/showAMobilePDFDoc.php');
							 $this->load->view('mobile/mobileFooter.php');
						}
				}	
			else if($mode == 'GraingerCDE')
				{
					if($mobileitem == null) 
						{
						  //Load header, Page, and Footer
						 $this->tw_data['pdfFilePath'] = base_url("pdf/graingerCDE.pdf"); 
						 $this->tw_data['pdfFileName'] = "graingerCDE.pdf";
						 $this->data['whichPage'] = "GraingerCDE";
						 $this->load->view('header/pageHeader',$this->data);
						 $this->load->view('showPDF/showAPDFDoc.php',$this->tw_data);
						 $this->load->view('rlsworks/frontFoot');
						}
					else if($mobileitem != null)
						{
							 $this->mdata['pdfFilePath'] = base_url("pdf/graingerCDE.pdf"); 
							 $this->mdata['pdfFileName'] = "graingerCDE.pdf";
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('showPDF/showAMobilePDFDoc.php');
							 $this->load->view('mobile/mobileFooter.php');
						}	
				}	
			else if($mode == 'mecProductManual')
				{
					 if($mobileitem == null) 
						{
							 //Load header, Page, and Footer
							 $this->tw_data['pdfFilePath'] = base_url("pdf/mecProductManual.pdf");
							 $this->tw_data['pdfFileName'] = "mecProductManual.pdf";
							 $this->data['whichPage'] = "mecProductManual";
							 $this->load->view('header/pageHeader',$this->data);
							 $this->load->view('showPDF/showAPDFDoc.php',$this->tw_data);
							 $this->load->view('rlsworks/frontFoot');
						}
					else if($mobileitem != null)
						{
							 
							 $this->mdata['pdfFilePath'] = base_url("pdf/mecProductManual.pdf");
							 $this->mdata['pdfFileName'] = "mecProductManual.pdf";
							 $this->load->view('mobile/mobileHeader.php',$this->mdata);
							 $this->load->view('showPDF/showAMobilePDFDoc.php');
							 $this->load->view('mobile/mobileFooter.php');
						}		
						
				}	
			else if($mode == 'livingInVegas')
				{
				     // Send to header to load correct CSS files
				     $this->data['whichPage'] = "livingInLV";
					 // Load photo from database-access-model to arrays
				     $this->llv_data['vacationingPhotos'] = $this->appimages_model->get_vacationing();
					 $this->llv_data['funInTheParkPhotos'] = $this->appimages_model->get_funinthepark();
					 $this->llv_data['localRecreationPhotos'] = $this->appimages_model->get_localrecreation();
					 $this->llv_data['localAmusementPhotos'] = $this->appimages_model->get_localamusement();
					 $this->llv_data['tennisTownPhotos'] = $this->appimages_model->get_tennistown();
					 
					 $this->llv_data['headerImages'] = $this->appimages_model->get_desktop_images();
					 
					 //Load header, Page, and Footer
					 $this->load->view('header/pageHeader',$this->data);
					 $this->load->view('livingInVegas/livingInLV.php',$this->llv_data);
					 $this->load->view('rlsworks/frontFoot');
				}		
				
				
			else if($mode == 'playbegin')
				{
				
				//echo $appType;
			    $this->load->helper('form');
                $this->load->library('form_validation');
                $this->form_validation->set_rules('xvalue', 'X Value', 'required');
                $this->form_validation->set_rules('yvalue', 'Y Value', 'required');
                
                $x = $this->input->post('xvalue');
                $y = $this->input->post('yvalue');
                $this->data["flag"] = 'no'; 
								
                if($this->form_validation->run() === FALSE)
                     {
                         // Display Last Board if Errors
                         $this->load->view('othello/header',$this->othello_data);
                         $this->load->view('othello/myboard');
                         $this->load->view('othello/footer');
                     }
                 
                 
				 
                 else if( $submit == 'Press to Play' )
                      {   
							  $isAjax = $this->othello_data['othelloAjaxMode'];
                              $this->othello_data["display1"] = "block";
                              $this->othello_data["display2"] = "none";
                              $this->othello_data["display3"] = "block";
                              $this->othello_data["display4"] = "none";
                              
                              $symbol = $this->othello_data["symbol"];
							  
                              $this->othello_data["flag"] = 'no';
                              $this->othello_data["message"]="Your symbol is '".$symbol."'. -- Input Coordinates or "."<br/><div id='clkspace' style='display:inline;color:brown'>**Click A Space**</div>"." for Your Move.";
                              $this->printBoard($this->othello_data["twoDimBoard"],$this->othello_data["cells"],$this->othello_data["doc"]);	// Generate Board to html file
                              // Send to header to load correct CSS files
							  $this->data['whichPage'] = "othello";
							  
							  
							  
							if($isAjax == 'no')
								{
								  $this->othello_data['othelloAjaxMode'] = 'no';
								  //Load header, Page, and Footer
								  $this->load->view('header/pageHeader',$this->data);
								  $this->load->view('othello/header',$this->othello_data);
								  $this->load->view('othello/myinitboard');
								  $this->load->view('othello/footer');   
								  $this->load->view('rlsworks/frontFoot'); 
								}
							else if($isAjax == 'yes')
								{
								  	  $this->othello_data['othelloAjaxMode'] = 'yes'; 
									  $this->load->view('othello/header',$this->othello_data);
									  $this->load->view('othello/myinitboard');
								      $this->load->view('othello/footer');  
								} 
                     }
             else if( $submit == 'Press to Continue' )
                      {
							  $isAjax = $this->othello_data['othelloAjaxMode'];
                              if($this->othello_data["symbol"]== 'X')
                                        $this->othello_data["symbol"]= 'O';
                              else if($this->othello_data["symbol"]== 'O')
                                        $this->othello_data["symbol"]= 'X';
                             
                              $this->com_player($isAjax);         
                       
                  }  
            else if($submit == 'Submit This Move')
                      {    
							$isAjax = $this->othello_data['othelloAjaxMode'];
							$this->playermove($x,$y,$this->input->post('hid'),$isAjax);
							
                      }      
          
      
				
				
				
				}
        }
    
    
	function processEmail()
		{  
		
				$this->load->helper('form');
                $this->load->library('form_validation');
                $this->form_validation->set_rules('nameField', 'Your Name', 'required');
				$this->form_validation->set_rules('toField', 'Send To', 'required');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
				$this->form_validation->set_rules('mess', 'Message', 'required');
				    
                $name = $this->input->post('nameField');
				$from = $this->input->post('fromField');
				$to = $this->input->post('toField');
                $bcc = $this->input->post('bccFielsd');
                $cc = $this->input->post('ccField');
                $subject = $this->input->post('subject');    
                $message = $this->input->post('mess');
				$emailType = $this->input->post('emailType');
			
				$this->email->from($name, 'Your Name');
				
				if($emailType == 'email1')
					{
						$this->email->to('Rsypertjr@hotmail.com'); 
						echo "Email Sent to rsypertjr@hotmail.com";
					}
				else if($emailType == 'email2')
					{
						$this->email->to('rsypertjr@gmail.com'); 
						echo "Email Sent to rsypertjr@gmail.com";
					}
				else
					{
						$this->email->to('Rsypertjr@hotmail.com'); 
						echo "Email Sent to rsypertjr@hotmail.com";
					}
					
				$this->email->cc($cc); 
				$this->email->bcc($bcc); 

				$this->email->subject($subject);
				$this->email->message($message);	

				$this->email->send();
				//echo $this->email->print_debugger();
		}
	
	
    function playbegin()
    {  
	          
                $this->load->helper('form');
                $this->load->library('form_validation');
                $this->form_validation->set_rules('xvalue', 'X Value', 'required');
                $this->form_validation->set_rules('yvalue', 'Y Value', 'required');
                
                $x = $this->input->post('xvalue');
                $y = $this->input->post('yvalue');
                $this->othello_data["flag"] = 'no'; 
				
				 
                 if($this->form_validation->run() === FALSE)
                     {
                         // Display Last Board if Errors
                         $this->load->view('othello/header',$this->data);
                         $this->load->view('othello/myboard');
                         $this->load->view('othello/footer');
                 }
                 
                
              else if( $this->input->post('submit') == 'Press to Play' )
                      {    
                              $this->othello_data["display1"] = "block";
                              $this->othello_data["display2"] = "none";
                              $this->othello_data["display3"] = "block";
                              $this->othello_data["display4"] = "none";
                              
                              $symbol = $this->othello_data["symbol"];
                              
                              $this->othello_data["flag"] = 'no';
                              $this->othello_data["message"]="Your symbol is '".$symbol."'. -- Input Coordinates or "."<br/><div id='clkspace' style='display:inline;color:brown'>**Click A Space**</div>"." for Your Move.";
                              $this->printBoard($this->othello_data["twoDimBoard"],$this->othello_data["cells"],$this->othello_data["doc"]);	// Generate Board to html file
                              // Send to header to load correct CSS files
							  $this->data['whichPage'] = "othello";
							  //Load header, Page, and Footer
							  $this->load->view('header/pageHeader',$this->data);
							  $this->load->view('othello/header',$this->othellodata);
                              $this->load->view('othello/myboard');
                              $this->load->view('othello/footer');     
                              
                     }
             else if( $this->input->post('submit') == 'Press to Continue' )
                      {
                              if($this->othello_data["symbol"]== 'X')
                                        $this->othello_data["symbol"]= 'O';
                              else if($this->othello_data["symbol"]== 'O')
                                        $this->othello_data["symbol"]= 'X';
                             
                              $this->com_player();         
                       
                  }  
            else if($this->input->post('submit') == 'Submit This Move')
                      {    
							$this->playermove($x,$y,$this->input->post('hid') );
                      }      
          
      
        
        }
    
    
        


function check()
{
        header('Content-type: application/x-javascript');
        function execute() 
        {
            jQFunction();
        }
        execute();

    }  
    

    
   function getboardInverse()
   {
   	       $file = "";
           //$file = file_get_contents("application/views/othello/thebody2.php");
           //file_put_contents("application/views/othello/myboard.php",$file);
           
           	$file = $this->selectTable('theBody2'); 
		   
			$myboard = $file;
			$this->console_log($myboard);
			$this->updateMyBoard($myboard);
		  
            //$myboard= $this->load->view("othello/myboard","",TRUE);
            $doc = new DOMDocument();
            $doc->loadHTML($myboard);
         
            $cells= array(); 
            $cellArray = array();
            for($i=1;$i<=8;$i++)
                {
                       $rows = $doc->getElementById("row".$i);
                       $cells = $rows->getElementsByTagName("div");
                      $j= 0;
                    foreach($cells as $cell)
                          {
                            $cellArray[$j][$i-1] = $cell->nodeValue;          //Flip Board Indices between players
                                                                                                     // Because computer player algorithm is nested loop          
                            
                           $j++;
                        }                  
            }   
       
            return $cellArray;
       }
    
   function com_player($isAjax)
       {
        // Retrieving the board
           $this->othello_data["twoDimBoard"] = $this->getboardInverse();
           $name = "boardArray";
 
        //  print_r($this->othello_data["twoDimBoard"]);  
           
           $this->othello_data["emptySpaces"] = $this->getEmptySpaces($this->othello_data["twoDimBoard"]);
          // print_r($this->othello_data["emptySpaces"]);
           $this->othello_data["possibleMoves"] = $this->getPossibleMoves($this->othello_data["emptySpaces"],$this->othello_data["symbol"],$this->othello_data["twoDimBoard"]);  // Get Open Spaces on Board for moves
          //print_r($this->othello_data["possibleMoves"]);
           
            if(count($this->othello_data["possibleMoves"]) > 0)   // If good moves exist
            {
              foreach($this->othello_data["possibleMoves"]  as $move)
                    {   
					  $x = $move[0];
					  $y = $move[1];
					  $this->othello_data["symbol"] = $move[2];
					  $symbol =  $this->othello_data["symbol"];
					  $oflipSpaces = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
					  // print_r($this->othello_data["flipSpaces"]);
					   
					  if($oflipSpaces != null)
								{
									$oflipSpaces = array_slice( $oflipSpaces ,1, (count( $oflipSpaces ) -1)   );
									//print_r($oflipSpaces);
								}
					 
							  foreach($oflipSpaces as $boardspace)
									{
										$x = $boardspace[0];
										$y = $boardspace[1];
										$symbol =  $boardspace[2];
					
										$this->othello_data["flipSpaces"] = $this->verifyMove($x,$y,  $symbol ,$this->othello_data["twoDimBoard"]);
										$this->othello_data["flipSpaces"] = array_slice($this->othello_data["flipSpaces"],1, (count($this->othello_data["flipSpaces"]) -1)   );
										 
										// print_r($this->othello_data["flipSpaces"] );
										if( isset($this->othello_data["flipSpaces"][2]))
											{ 
												   $this->othello_data["twoDimBoard"] = $this->updateGameBoard($this->othello_data["flipSpaces"],$this->othello_data["twoDimBoard"]); // Update Game Board	
												   $this->printBoard($this->othello_data["twoDimBoard"],$this->othello_data["cells"],$this->othello_data["doc"]);	// Generate Board to html file
											  
												 // Display Board
													  $this->othello_data["display1"] = "block";
													  $this->othello_data["display2"] = "none";
													  $this->othello_data["display3"] = "block";
													  $this->othello_data["display4"] = "none";
									  
												  $symbol = $this->othello_data["symbol"];
												  if($symbol == 'X')
																$this->othello_data["symbol"]= 'O';
												  else if($symbol == 'O')
																$this->othello_data["symbol"]= 'X';
																
												  $symbol = $this->othello_data["symbol"];
												  $this->othello_data["flag"] = 'yes';  
												 // $this->othello_data["cheight"] = 75;
												  $this->othello_data["message"]="Your symbol is '".$symbol."'. ------ Input Coordinates for Your Move.";
												 
												    // Send to header to load correct CSS files
													 $this->data['whichPage'] = "othello";
												
												 	if($isAjax == 'no')
														{
														  $this->data['othelloAjaxMode'] = 'no'; 
														  //Load header, Page, and Footer
														  $this->load->view('header/pageHeader',$this->data);
														  $this->load->view('othello/header',$this->othello_data);
														  //$this->load->view('othello/myboard');
														  $this->load->view('othello/myboardDB');
														  $this->load->view('othello/footer'); 
														}
													else if($isAjax == 'yes')
														{
															 $this->data['othelloAjaxMode'] = 'yes'; 
															 $this->load->view('othello/header',$this->othello_data);
															 //$this->load->view('othello/myboard');
														     $this->load->view('othello/myboardDB');
															 $this->load->view('othello/footer'); 
														}
																
												 
											   // Save Board to File
											   $file = "";
											   //$file = file_get_contents("application/views/othello/myboard.php");
											   //file_put_contents("application/views/othello/thebody2.php",$file);
											  
											   $file = $this->selectTable('myBoard');
											   $GLOBALS['myBoard'] = $file; 
											   $this->console_log($file);
											   $this->updateTheBody2($file);
											   //$file2 = $this->selectTable('theBody2'); 
											   //$this->console_log($file2); 
												
										       if($symbol == 'X')
																$this->othello_data["symbol"]= 'O';
											    else if($symbol == 'O')
																$this->othello_data["symbol"]= 'X';
												 return;
										   }
									}  

								
								
                     }
					 	  $finScore = $this->calculateFinalScore();
						  if($finScore['O'] > $finScore['X'])
								$result = "SHUCKS!! YOU WIN";
						  else
								$result = "HA!  HA!  I WIN!!!!!!!";
						  $this->othello_data["message"]= "<span id='prbutton' style='color:purple;font-size:150%'>!!!GAME OVER!!!</span> <br/>I CAN'T MOVE AGAIN<br/><span style='color:purple'> The Score is:<br/>O: ".$finScore['O'].' X: '.$finScore['X']."</span><br/>".$result;
						
						   $this->othello_data["display2"] = "none";
						   $this->othello_data["display3"] = "block";
						   $this->othello_data["display4"] = "none";
						   $this->othello_data["display1"] = "none";
						   //$this->othello_data["cheight"]= 75;
						 
						     // Send to header to load correct CSS files
							 	if($isAjax == 'no')
									{
									  $this->othello_data['othelloAjaxMode'] = 'no'; 
									  //Load header, Page, and Footer
									  $this->load->view('header/pageHeader',$this->data);
									  $this->load->view('othello/header',$this->othello_data);
									  //$this->load->view('othello/myboard');
									  $this->load->view('othello/myboardDB'); 
									  $this->load->view('othello/footer'); 
									}
								else if($isAjax == 'yes')
									{
										 $this->othello_data['othelloAjaxMode'] = 'yes'; 
										 $this->load->view('othello/header',$this->othello_data);
										 //$this->load->view('othello/myboard');
										 $this->load->view('othello/myboardDB'); 
										 $this->load->view('othello/footer'); 
									}


                }
            else  
                      {
                              $finScore = $this->calculateFinalScore();
                              if($finScore['O'] > $finScore['X'])
                                    $result = "SHUCKS!! YOU WIN";
                              else
                                    $result = "HA!  HA!  I WIN!!!!!!!";
                              $this->othello_data["message"]= "<span id='prbutton' style='color:purple;font-size:150%'>!!!GAME OVER!!!</span> <br/>I CAN'T MOVE AGAIN<br/><span style='color:purple'> The Score is:<br/>O: ".$finScore['O'].' X: '.$finScore['X']."</span><br/>".$result;
                            
                               $this->othello_data["display2"] = "none";
                               $this->othello_data["display3"] = "block";
                               $this->othello_data["display4"] = "none";
                               $this->othello_data["display1"] = "none";
                               //$this->othello_data["cheight"]= 75;
                             
                                 // Send to header to load correct CSS files
								 $this->data['whichPage'] = "othello";
									if($isAjax == 'no')
										{
										  $this->othello_data['othelloAjaxMode'] = 'no'; 
										  //Load header, Page, and Footer
										  $this->load->view('header/pageHeader',$this->data);
										  $this->load->view('othello/header',$this->othello_data);
										  //$this->load->view('othello/myboard');
										  $this->load->view('othello/myboardDB');
										  $this->load->view('othello/footer'); 
										}
									else if($isAjax == 'yes')
										{
											 $this->data['othelloAjaxMode'] = 'yes'; 
											 $this->load->view('othello/header',$this->othello_data);
											 //$this->load->view('othello/myboard');
											 $this->load->view('othello/myboardDB');
											 $this->load->view('othello/footer'); 
										}
                        
                        }
                 
        }
  
  
function playermove($x,$y,$flag,$isAjax)
{        

			$this->load->helper('form');
            if($flag == 'yes')
                {
                        // Retrieving Board
                       $this->othello_data["twoDimBoard"] = $this->getboardInverse();
                }
       
            
            $symbol = $this->othello_data["symbol"];
         
           $oflipSpaces = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
         
           if($oflipSpaces != null )
                        {
                            $oflipSpaces = array_slice( $oflipSpaces ,1, (count( $oflipSpaces ) -1)   );
                        }
                    $this->load->library('javascript');
         if(isset($oflipSpaces [2]))
            {    
               foreach($oflipSpaces as $boardspace)
                    {
                     
                        $x = $boardspace[0];
                        $y = $boardspace[1];
                        $symbol =  $boardspace[2]; 
                        
                        $this->othello_data["flipSpaces"] = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
                       //print_r($this->othello_data["flipSpaces"]);
                        if($this->othello_data["flipSpaces"] != null &&  (count($this->othello_data["flipSpaces"]) >= 2 ))
                                {
                                    $this->othello_data["flipSpaces"] = array_slice($this->othello_data["flipSpaces"],1, (count($this->othello_data["flipSpaces"]) -1)   );
                                }

                        if( isset($this->othello_data["flipSpaces"][2]))
                                {    
                                       $this->othello_data["twoDimBoard"] = $this->updateGameBoard($this->othello_data["flipSpaces"],$this->othello_data["twoDimBoard"]); // Update Game Board	
                                      
                                      // Generate Board
                                       $this->printBoard($this->othello_data["twoDimBoard"],$this->othello_data["cells"],$this->othello_data["doc"]);	// Generate Board to html file
                                  
                                        $this->othello_data["display1"] = "none";
                                        $this->othello_data["display2"] = "block";
                                        $this->othello_data["display3"] = "none";
                                        $this->othello_data["display4"] = "block";

										//$this->othello_data["cheight"] = 75;
                                        $this->othello_data["message"] ="This is Your move with symbol: '".$this->othello_data["symbol"]."'.<br/><span id='prbutton' style='color:purple'>**Press Button for Computer Move**</span>"; 
                                        $this->othello_data["flag"] = 'no';
										
                                       // Display Board
									     // Send to header to load correct CSS files
										 $this->data['whichPage'] = "othello";
										if($isAjax == 'no')
											{
											  $this->othello_data['othelloAjaxMode'] = 'no'; 
											  //Load header, Page, and Footer
											  $this->load->view('header/pageHeader',$this->data);
											  $this->load->view('othello/header',$this->othello_data);
											  //$this->load->view('othello/myboard');
											 $this->load->view('othello/myboardDB');
											 
											 $this->load->view('othello/footer'); 
											}
										else if($isAjax == 'yes')
											{
												 $this->data['othelloAjaxMode'] = 'yes'; 
												 $this->load->view('othello/header',$this->othello_data);
												 //$this->load->view('othello/myboard');
												 $this->load->view('othello/myboardDB'); 
												 $this->load->view('othello/footer'); 
											}
                                      
                                      // Save Board to File
                                       $file = "";
                                       //$file = file_get_contents("application/views/othello/myboard.php");
                                       //file_put_contents("application/views/othello/thebody2.php",$file);
                                       $file = $this->selectTable('myBoard');
                                       $this->console_log($file);
                                       $GLOBALS['myBoard'] = $file;
									   $this->updateTheBody2($file);
									   //$file2 = $this->selectTable('theBody2'); 
									   //$this->console_log($file2); 
                                   
                            }
                      
                    }
            }  
            else 
            {
           // Determine if any moves left     
           $this->othello_data["emptySpaces"] = $this->getEmptySpaces($this->othello_data["twoDimBoard"]);
          // print_r($this->othello_data["emptySpaces"]);
           $this->othello_data["possibleMoves"] = $this->getPossibleMoves($this->othello_data["emptySpaces"],$this->othello_data["symbol"],$this->othello_data["twoDimBoard"]);  // Get Open Spaces on Board for moves
          //print_r($this->othello_data["possibleMoves"]);      
                
            $isThereStillAMove = false;
            foreach($this->othello_data["possibleMoves"]  as $move)
                    {   
                                  $x = $move[0];
                                  $y = $move[1];
                                  $this->othello_data["symbol"] = $move[2];
                                  $symbol =  $this->othello_data["symbol"];
                                  $oflipSpaces = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
                                   //print_r($this->othello_data["flipSpaces"]);
                                   
                                  if($oflipSpaces != null)
                                            {
                                                $oflipSpaces = array_slice( $oflipSpaces ,1, (count( $oflipSpaces ) -1)   );
                                        }
                                 if(count($oflipSpaces) > 0)
                                    $isThereStillAMove = true;
            
                     }
                
             
         
          
           if($isThereStillAMove == true )
				{
				    $this->othello_data["flag"] = 'yes'; 
                 	$this->othello_data["message"]= "<span id='prbutton3' style='color:purple;'>!!!NOT A GOOD MOVE-TRY AGAIN!!!</span> <br/>Your symbol is '".$this->othello_data["symbol"]."'";
				    $this->othello_data["display1"] = "block";
			        $this->othello_data["display2"] = "none";
				    $this->othello_data["display3"] = "block";
				    $this->othello_data["display4"] = "none";
			   }
		   else
                    {
                        
                     $finScore = $this->calculateFinalScore();
                      if($finScore['O'] > $finScore['X'])
                            $result = "SHUCKS!! YOU WIN";
                      else 
                            $result = "HA!  HA!  I WIN!!!!!!!";
                            
                      $espaces = $this->othello_data["emptySpaces"] = $this->getEmptySpaces($this->othello_data["twoDimBoard"]);     
                      if(count($espaces) > 0)
                          $result = "Since the Board is Not Full, The Highest Score Wins, Which Means <br/><span id='prbutton2' style='color:purple;font-size:150%'>".$result."</span>";
                     
                      $this->othello_data["message"]= "<span id='prbutton' style='color:purple;'>!!!GAME OVER!!!</span> <br/>It Appears You Have No Moves Left<br/><span style='color:purple'> The Score is:    O:".$finScore['O'].' X:'.$finScore['X']." </span><br/>".$result;
                      $this->othello_data["display1"] = "none";
                      $this->othello_data["display2"] = "none";
                      $this->othello_data["display3"] = "block";
                      $this->othello_data["display4"] = "none";
                    }
    
		  // Send to header to load correct CSS files
		 $this->data['whichPage'] = "othello";
		 //Load header, Page, and Footer
			if($isAjax == 'no')
				{
				  $this->data['othelloAjaxMode'] = 'no'; 
				  //Load header, Page, and Footer
				  $this->load->view('header/pageHeader',$this->data);
				  $this->load->view('othello/header',$this->othello_data);
				  //$this->load->view('othello/myboard');
				  $this->load->view('othello/myboardDB');
				  $this->load->view('othello/footer'); 
				}
			else if($isAjax == 'yes')
				{
					 $this->data['othelloAjaxMode'] = 'yes'; 
					 $this->load->view('othello/header',$this->othello_data);
				     //$this->load->view('othello/myboard');
				     $this->load->view('othello/myboardDB');
					 $this->load->view('othello/footer'); 
				}

            }
}
   


 
function badFirstMove($x,$y,$flag)
{        

			$this->load->helper('form');
            if($flag == 'yes')
                {
                        // Retrieving Board
                       $this->othello_data["twoDimBoard"] =$this->getboardInverse();
                       //print_r($this->othello_data["twoDimBoard"]);  
                }
                
            $symbol = $this->othello_data["symbol"];
         
           $oflipSpaces = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
          //echo "<br/> x: ".$x." y: ".$y." sym: ".$symbol;
         
           if($oflipSpaces != null )
                        {
                            $oflipSpaces = array_slice( $oflipSpaces ,1, (count( $oflipSpaces ) -1)   );
                        }
                    $this->load->library('javascript');
         if( isset($oflipSpaces [2]))
            {    
               foreach($oflipSpaces as $boardspace)
                    {
                     
                        $x = $boardspace[0];
                        $y = $boardspace[1];
                        $symbol =  $boardspace[2]; 
                        
                        $this->othello_data["flipSpaces"] = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
                       //print_r($this->othello_data["flipSpaces"]);
                        if($this->othello_data["flipSpaces"] != null &&  (count($this->othello_data["flipSpaces"]) >= 2 ))
                                {
                                    $this->othello_data["flipSpaces"] = array_slice($this->othello_data["flipSpaces"],1, (count($this->othello_data["flipSpaces"]) -1)   );
                                }

                        if( isset($this->othello_data["flipSpaces"][2]))
                                {    
                                       $this->othello_data["twoDimBoard"] = $this->updateGameBoard($this->othello_data["flipSpaces"],$this->othello_data["twoDimBoard"]); // Update Game Board	
                                       //print_r( $this->othello_data["twoDimBoard"] );
                                     
                                      // Generate Board
                                       $this->printBoard($this->othello_data["twoDimBoard"],$this->othello_data["cells"],$this->othello_data["doc"]);	// Generate Board to html file
                                  
                                        $this->othello_data["display1"] = "none";
                                        $this->othello_data["display2"] = "block";
                                        $this->othello_data["display3"] = "none";
                                        $this->othello_data["display4"] = "block";

										//$this->othello_data["cheight"] = 75;
                                        $this->othello_data["message"] ="This is Your move with symbol: '".$this->othello_data["symbol"]."'.<br/><span id='prbutton' style='color:purple'>**Press Button for Computer Move**</span>"; 
                                        $this->othello_data["flag"] = 'no';
										
                                       // Display Board
                                        $this->load->view('othello/header',$this->othello_data);
                                        $this->load->view('othello/myinitboard');
                                        $this->load->view('othello/footer');  
                                      
                                      // Save Board to File
                                       $file = "";
                                       //$file = file_get_contents("application/views/othello/myboard.php");
                                       //file_put_contents("application/views/othello/thebody2.php",$file);
                                       $file = $this->selectTable('myBoard');
                                       $GLOBALS['myBoard'] = $file; 
									   //$this->console_log($GLOBALS['myBoard']);
                                       //print_r($GLOBALS['myBoard'] );
									   $this->updateTheBody2($file);
                                  
                            }
                      
                    }
            }  
            else 
            {
           // Determine if any moves left     
           $this->othello_data["emptySpaces"] = $this->getEmptySpaces($this->othello_data["twoDimBoard"]);
          // print_r($this->othello_data["emptySpaces"]);
           $this->othello_data["possibleMoves"] = $this->getPossibleMoves($this->othello_data["emptySpaces"],$this->othello_data["symbol"],$this->othello_data["twoDimBoard"]);  // Get Open Spaces on Board for moves
          //print_r($this->othello_data["possibleMoves"]);      
                
            $isThereStillAMove = false;
            foreach($this->othello_data["possibleMoves"]  as $move)
                    {   
                                  $x = $move[0];
                                  $y = $move[1];
                                  $this->othello_data["symbol"] = $move[2];
                                  $symbol =  $this->othello_data["symbol"];
                                  $oflipSpaces = $this->verifyMove($x,$y,$symbol,$this->othello_data["twoDimBoard"]);
                                   //print_r($this->othello_data["flipSpaces"]);
                                   
                                  if($oflipSpaces != null)
                                            {
                                                $oflipSpaces = array_slice( $oflipSpaces ,1, (count( $oflipSpaces ) -1)   );
                                        }
                                 if(count($oflipSpaces) > 0)
                                    $isThereStillAMove = true;
            
                     }
                
             
         
          
           if($isThereStillAMove == true )
				{
				    $this->othello_data["flag"] = 'yes'; 
                   // $this->othello_data["message"] = "<span id='prbutton3' style='color:purple;'>"."NOT A GOOD MOVE--TRY AGAIN !!!</span>--Your symbol is '".$this->othello_data["symbol"]."'";
					$this->othello_data["message"]= "<span id='prbutton3' style='color:purple;'>!!!NOT A GOOD MOVE-TRY AGAIN!!!</span> <br/>Your symbol is '".$this->othello_data["symbol"]."'";
				    $this->othello_data["display1"] = "block";
			        $this->othello_data["display2"] = "none";
				    $this->othello_data["display3"] = "block";
				    $this->othello_data["display4"] = "none";
			   }
		   else
                    {
                        
                     $finScore = $this->calculateFinalScore();
                      if($finScore['O'] > $finScore['X'])
                            $result = "SHUCKS!! YOU WIN";
                      else 
                            $result = "HA!  HA!  I WIN!!!!!!!";
                            
                      $espaces = $this->othello_data["emptySpaces"] = $this->getEmptySpaces($this->othello_data["twoDimBoard"]);     
                      if(count($espaces) > 0)
                          $result = "Since the Board is Not Full, The Highest Score Wins, Which Means <br/><span id='prbutton2' style='color:purple;font-size:150%'>".$result."</span>";
                     
                      $this->othello_data["message"]= "<span id='prbutton' style='color:purple;'>!!!GAME OVER!!!</span> <br/>It Appears You Have No Moves Left<br/><span style='color:purple'> The Score is:    O:".$finScore['O'].' X:'.$finScore['X']." </span><br/>".$result;
                      $this->othello_data["display1"] = "none";
                      $this->othello_data["display2"] = "none";
                      $this->othello_data["display3"] = "block";
                      $this->othello_data["display4"] = "none";
                    }
    
          /* $this->load->view('othello/header',$this->othello_data);
		   $this->load->view('othello/myboard');
           $this->load->view('othello/footer'); */
           
           
             // Send to header to load correct CSS files
		    $this->data['whichPage'] = "othello";
           	if($isAjax == 'no')
				{
				  $this->data['othelloAjaxMode'] = 'no'; 
				  //Load header, Page, and Footer
				  $this->load->view('header/pageHeader',$this->data);
				  $this->load->view('othello/header',$this->othello_data);
				  //$this->load->view('othello/myboard');
				  $this->load->view('othello/myboardDB');
				  $this->load->view('othello/footer'); 
				}
			else if($isAjax == 'yes')
				{
					 $this->data['othelloAjaxMode'] = 'yes'; 
					 $this->load->view('othello/header',$this->othello_data);
					 //$this->load->view('othello/myboard');
					 $this->load->view('othello/myboardDB');
					 $this->load->view('othello/footer'); 
				}


            }
}
















   

function printBoard($twDimBoard,$cells,$dc)
{
	for($i=0;$i<8;$i++)
		{
                $j=0;
                foreach($cells[$i] as $cell)
                    {
                    $cell->nodeValue =  $twDimBoard[$j][$i];  //Transpose rows to columns
                    $this->othello_data["cellArray"][$i][$j] = $twDimBoard[$i][$j]; 
                    $j++;
                }
        }
        
        $this->othello_data["twoDimBoard"] = $this->othello_data["cellArray"];
        $thebody = $dc->getElementById('gboard');
		//file_put_contents("application/views/othello/myboard.php",$dc->saveHTML($thebody));
		$dcHTML = $dc->saveHTML($thebody);
		$this->updateMyBoard($dcHTML);
		$GLOBALS['myBoard'] = $this->selectTable('myBoard');
		//print_r($GLOBALS['myBoard']);
		
}	
 
   
public function message($to = 'World')
        {
            echo "Hello {$to}!".PHP_EOL;
        }
    
    
public function getTwoDimBoard($boardRows)
    {
           $boardRowArr = array();
            $twoDBoard = array();
            for( $i=0;$i < 8;$i++)
                {
                    $boardRowArr = str_split($boardRows[$i],1);
                    
                    //print_r($boardRows[$i]);
                    for( $j=0;$j<count($boardRowArr);$j++)
                        {
                            $twoDBoard[$i][$j] = $boardRowArr[$j];
                            
                        }
                }
                return $twoDBoard;  
    }
    

public function getTwoDimBoardSpace($x, $y, $twDimBoard)
       {
            for( $i=0;$i < 8;$i++)
                {
                    for( $j=0;$j<8;$j++)
                        {
                            if($i == $x && $j==$y)
                                return $twDimBoard[$i][$j];
                        }
                }

        }
    
  
function getEmptySpaces($twDimBoard)
        {
            $emptySpaces = array();
            $count = 0;
            for($i=0;$i<8;$i++)
                {
                    for( $j=0;$j<8;$j++)
                        {
                            if($twDimBoard[$i][$j] == '.')
                                {
                                    $emptySpaces[$count][0]=$i;
                                    $emptySpaces[$count][1]=$j;
                                    ++$count;
                                }
                        }
                }
            return $emptySpaces;
        }
  


function getPossibleMoves($emptySpaces,$symbol,$twDimBoard)
    {
        
        //echo count($emptySpaces);
        $goodMoves = array();
        $x=0;
        $y=0;
        $count = 0;
        //echo count($emptySpaces);
        for($i=0;$i<count($emptySpaces);$i++)
            {
                $x = $emptySpaces[$i][0];
                //
                $y = $emptySpaces[$i][1];
                $this->othello_data["flipSpaces"] = $this->verifyMove($x,$y,$symbol,$twDimBoard); 
                
               if(count($this->othello_data["flipSpaces"]) >=1 )
                    {  
                        $goodMoves[$count][0]=$x;
                        $goodMoves[$count][1]=$y;
                        $goodMoves[$count][2]=$symbol;
                        ++$count;
                     }
            }
           return $goodMoves;
}

  
  
  
// Send 2-d array with the first index being for another array of length 3 containing the x,y,symbol triplet
// This array is built after a move is submitted and several spaces on the board need to be changed (flipped) 
function updateGameBoard($xyArr,$twDimBoard)
{
	for($i = 0;$i < count($xyArr);$i++)
		{
					$x = $xyArr[$i][0];
					$y = $xyArr[$i][1];
					$symbol = $xyArr[$i][2];
					$twDimBoard = $this->changeGameBoardSpace($x,$y,$symbol,$twDimBoard);
			  
		}
		return $twDimBoard;
}
  
 
 
 function changeGameBoardSpace($x,$y,$symb,$twDimBoard)
{
		$twDimBoard[$x][$y] = $symb;		
	    return $twDimBoard;
}
 
 
  
  


 
function verifyMove($x,$y,$sym,$twDimBoard)
	{
	    if($sym == 'X')
			{
				$symb = 'X';
				$osymb = 'O';
			}
	    else if($sym == 'O')
			{
				$symb = 'O';
				$osymb = 'X';
			}
		$flipSpaces = array();
		$tempSpaces = array();
		$flipSpaces[0][0] = null;
		$flipSpaces[0][1] = null;
    	$flipSpaces[0][2] = null;

       
		//echo $symb;
		if($this->getGameBoardSpace($x,$y,$twDimBoard) == '.')  // Make sure move is to empty space
				{
					// Verify to the East
					$count = 1;
					$ix = $x;
					$iy = $y;
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
            
                    if(  ($ix+1)  < 8  )
                        {
                                while($this->getGameBoardSpace(++$ix,$iy, $twDimBoard) == $osymb)
                                    {
                                      $tempSpaces[$count][0] = $ix;
                                      $tempSpaces[$count][1] = $iy;
                                      $tempSpaces[$count][2] = $symb;
                                      $count++;
                                    }
                                if(($count > 1) && ($this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb))
                                        { 
                                            $tempSpaces[$count][0] = $ix;
                                            $tempSpaces[$count][1] = $iy;
                                            $tempSpaces[$count][2] = $symb;
                                            $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                        }
                                else
                                    $tempSpaces = null;
						}
						
					 // Verify to the North East
					$count = 1;
					$ix = $x;
					$iy = $y;
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
						
                     if(    ( ($ix+1)  < 8)  &&  ( ( $iy+1)  < 8)  )
                        {
                            while($this->getGameBoardSpace(++$ix,++$iy, $twDimBoard) == $osymb)
                                { 
                                  
                                  $tempSpaces[$count][0] = $ix;
                                  $tempSpaces[$count][1] = $iy;
                                  $tempSpaces[$count][2] = $symb;
                                  $count++;
                                }
                            if(($count > 1) && $this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb)
                                    {
                                       
                                        $tempSpaces[$count][0] = $ix;
                                        $tempSpaces[$count][1] = $iy;
                                        $tempSpaces[$count][2] = $symb;
                                        $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                    }
                            else
                                $tempSpaces = null;
                      }
						
					 // Verify to the North
					$count = 1;
					$ix = $x;
					$iy = $y;
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
				       
                    if(  ($iy+1)  < 8  )
                        {
                                while($this->getGameBoardSpace($ix,++$iy, $twDimBoard) == $osymb)
                                    {
                                      $tempSpaces[$count][0] = $ix;
                                      $tempSpaces[$count][1] = $iy;
                                      $tempSpaces[$count][2] = $symb;
                                      $count++;
                                }
                            
                              // echo $symb;
                                if(($count > 1) && $this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb  )  
                                        { 
                                            $tempSpaces[$count][0] = $ix;
                                            $tempSpaces[$count][1] = $iy;
                                            $tempSpaces[$count][2] = $symb;
                                            $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                        }
                                else
                                    $tempSpaces = null;
                        }
					
					// Verify to the North West
					$count = 1;
					$ix = $x;
					$iy = $y;
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
                    
                      if(    ( ($ix-1)  > 0)  &&  ( ( $iy+1) < 8)  )
                        {
										
                            while($this->getGameBoardSpace(--$ix,++$iy, $twDimBoard) == $osymb)
                                {
                                  $tempSpaces[$count][0] = $ix;
                                  $tempSpaces[$count][1] = $iy;
                                  $tempSpaces[$count][2] = $symb;
                                  $count++;
                                }
                            if(($count > 1) && ($this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb))
                                    {
                                        $tempSpaces[$count][0] = $ix;
                                        $tempSpaces[$count][1] = $iy;
                                        $tempSpaces[$count][2] = $symb; 
                                        $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                    }
                            else
                                $tempSpaces = null;
                        }
					
					
					// Verify to the West
					$count = 1;
					$ix = $x;
					$iy = $y;
					
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
                    
                      if(  ($ix-1)  > 0  )
                        {
					
                                while($this->getGameBoardSpace(--$ix,$iy, $twDimBoard) == $osymb)
                                    {
                                      $tempSpaces[$count][0] = $ix;
                                      $tempSpaces[$count][1] = $iy;
                                      $tempSpaces[$count][2] = $symb;
                                      $count++;
                                    }
                                if(($count > 1) && ($this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb))
                                        {
                                            $tempSpaces[$count][0] = $ix;
                                            $tempSpaces[$count][1] = $iy;
                                            $tempSpaces[$count][2] = $symb;
                                            $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                        }
                                else
                                    $tempSpaces = null;
                            }
						
					// Verify to the South West
					$count = 1;
					$ix = $x;
					$iy = $y;
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
                             
                             
                      if(    ( ($ix-1)  > 0)  &&  ( ( $iy-1) > 0)  )
                        {
										
                                while($this->getGameBoardSpace(--$ix,--$iy, $twDimBoard) == $osymb)
                                    {
                                      $tempSpaces[$count][0] = $ix;
                                      $tempSpaces[$count][1] = $iy;
                                      $tempSpaces[$count][2] = $symb;
                                      $count++;
                                    }
                                if(($count > 1) && ($this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb))
                                        {
                                            $tempSpaces[$count][0] = $ix;
                                            $tempSpaces[$count][1] = $iy;
                                            $tempSpaces[$count][2] = $symb; 
                                            $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                        }
                                else
                                    $tempSpaces = null;
                        }
					
				
			      // Verify to the South 
					$count = 1;
					$ix = $x;
					$iy = $y;
					//echo "x= ".$ix." y= ".$iy." other symb= ".$osymb."</br>";
					
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
                    
                       if(  ($iy-1)  > 0  )
                        {
                            
                            while($this->getGameBoardSpace($ix,--$iy,$twDimBoard) == $osymb)
                                {
                                  $tempSpaces[$count][0] = $ix;
                                  $tempSpaces[$count][1] = $iy;
                                  $tempSpaces[$count][2] = $symb;
                                  $count++;
                                }
                            if(($count > 1) && ($this->getGameBoardSpace($ix,$iy,$twDimBoard) == $symb))
                                    {
                                        $tempSpaces[$count][0] = $ix;
                                        $tempSpaces[$count][1] = $iy;
                                        $tempSpaces[$count][2] = $symb;
                                        $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                        
                                    }
                            else
                                $tempSpaces = null;
                        }
                    
			
					// Verify to the South East 
					$count = 1;
					$ix = $x;
					$iy = $y;
					$tempSpaces[0][0] = $x;
				    $tempSpaces[0][1] = $y;
				    $tempSpaces[0][2] = $symb;
                               
                      if(    ( ($ix+1)  < 8)  &&  ( ( $iy-1) > 0)  )
                        {
                                    while($this->getGameBoardSpace(++$ix,--$iy, $twDimBoard) == $osymb)
                                        {
                                          $tempSpaces[$count][0] = $ix;
                                          $tempSpaces[$count][1] = $iy;
                                          $tempSpaces[$count][2] = $symb;
                                          $count++;
                                        }
                                    if(($count > 1) && ($this->getGameBoardSpace($ix,$iy, $twDimBoard) == $symb))
                                            {
                                                  $tempSpaces[$count][0] = $ix;
                                                  $tempSpaces[$count][1] = $iy;
                                                  $tempSpaces[$count][2] = $symb;
                                                  $flipSpaces = array_merge($flipSpaces,$tempSpaces);
                                            }
                                    else
                                        $tempSpaces = null;
                        }
						
						
				if($flipSpaces != null)
					{
						return $flipSpaces; 
					}					
				else
					return null;
			
				}
				else 
					return null;
	}

	 
  
  
 
function getGameBoardSpace($x, $y, $twDimBoard)
    {
         //echo "<br/> x: ".$x." y: ".$y." sym: ".$twDimBoard[$y][$x];
         if(($x < 0) || ($x >= 8) || ($y<0) || ($y >= 8))
                return '@';
                
        $boardRowArr = array();
       //echo $twDimBoard[$y][$x];
       
        return $twDimBoard[$x][$y];
    } 
  
  
 function calculateFinalScore()
    {
        $board = $this->getBoardInverse();
        $Ocount = 0;
        $Xcount = 0;
       
        for($i=0;$i<8;$i++)
            for($j=0;$j<8;$j++)
                    {
                            if($board[$i][$j] == 'O')
                                $Ocount += 1;
                            else if($board[$i][$j] == 'X')
                                $Xcount += 1;
                        
                    }
       $finalScore = array('O' => $Ocount,'X' =>$Xcount,);
        return $finalScore;
        
    } 
   
  function updateInitBoard($initBoard){
            $servername = "localhost";
			$username = "rlsworks_richard";
			$password = "syp3rt";
			$dbname = "rlsworks_app_images";
            
            $conn = mysqli_connect($servername,$username,$password, $dbname);
           // Check connection
			if (!$conn) {
			    die("Connection failed: " . mysqli_connect_error());
			} 
			
			
			$initBoard = base64_encode($initBoard);	
			$sql = 'UPDATE boards SET initBoard = "'.$initBoard.'" WHERE rowNum=1';
			//$this->console_log($sql);
          
            if (mysqli_query($conn, $sql)) {
			    $this->console_log("initBoard Table updated successfully");
			} else {
			    $this->console_log("Error: " . $sql . "<br>" . mysqli_error($conn));
			}
            
            $conn->close();
  }    

  function updateMyBoard($myBoard){
            $servername = "localhost";
			$username = "rlsworks_richard";
			$password = "syp3rt";
			$dbname = "rlsworks_app_images";
            
            $conn = mysqli_connect($servername,$username,$password, $dbname);
           // Check connection
			if (!$conn) {
			    die("Connection failed: " . mysqli_connect_error());
			} 
			
			$myBoard = base64_encode($myBoard);		
			$sql = 'UPDATE boards SET myBoard = "'.$myBoard.'" WHERE rowNum=1';
			$this->console_log($sql);
          
            if (mysqli_query($conn, $sql)) {
			    $this->console_log("myBoard Table updated successfully");
			} else {
			    $this->console_log("Error: " . $sql . "<br>" . mysqli_error($conn));
			}
            
            $conn->close();
  }   
  
  
   function updateTheBody2($theBody2){
            $servername = "localhost";
			$username = "rlsworks_richard";
			$password = "syp3rt";
			$dbname = "rlsworks_app_images";
            
            $conn = mysqli_connect($servername,$username,$password, $dbname);
           // Check connection
			if (!$conn) {
			    die("Connection failed: " . mysqli_connect_error());
			} 
			
			$theBody2 = base64_encode($theBody2);		
			$sql = 'UPDATE boards SET theBody2 = "'.$theBody2.'" WHERE rowNum=1';
			//$this->console_log($sql);
          
            if (mysqli_query($conn, $sql)) {
			    $this->console_log("theBody2 Table updated successfully");
			} else {
			    $this->console_log("Error: " . $sql . "<br>" . mysqli_error($conn));
			}
            
            $conn->close();
  }   
  
  
  function selectTable($table){
  	    $servername = "localhost";
		$username = "rlsworks_richard";
		$password = "syp3rt";
		$dbname = "rlsworks_app_images";
		$file="";
        
        $conn = mysqli_connect($servername,$username,$password, $dbname);
       // Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		} 
		
		$sql = 'SELECT '.$table.' from boards WHERE rowNum=1';
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        $file = $row[$table];
		        $file = base64_decode($file);
		    }
		} else {
		    echo "0 results";
		}
        $conn->close(); 
           
        //$this->console_log($file);
        return $file;
  	
  }
  
    
 function console_log( $data ) {
	  $output  = "<script>console.log( 'PHP debugger: ";
	  $output .= json_encode(print_r($data, true));
	  $output .= "' );</script>";
	  echo $output;
}
    
}