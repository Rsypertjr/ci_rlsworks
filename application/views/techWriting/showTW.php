<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->


<script type="text/javascript">
		  $(document).ready(function(){
		
		    		
		      // Hide Buttons from display
					  $('#clickdiv1').hide();
					  $('#clickdiv2').hide();
					  $('#clickdiv3').hide();
					  $('#clickdiv4').hide();
					  $('#clickdiv5').hide(); 
					  $('#clickdiv6').hide(); 
							 
						// Prepare Description to Show
					  $('#description').css('color','green');
					  $('#description').css('font-size','1.8em');
					  $('#description').css('padding','20px');
					  $('#description').show(); 

					// Animate fade on background
					$(document).mousemove(function()
						{
						  $('#oframebg')
								.animate({opacity:"0.20"},2000)
								.animate({opacity:"0.70"},2000);
						
						 $('#oframe').css('opacity','1.0');
								
						
						});
					
					// Set up Hover over Button Display  and Default Description
					   $('.docbuttons').bind('mouseover',function()
						{
							$(this).css('background-color','tan');
							
						});	
							
					   $('.docbuttons').bind('mouseleave',function()
						{
							$(this).css('background-color','white');
							$('#description').css('text-align','center');
							$('#description').html('Hover over Document Title for its Descripition. <p><span style="color:green;font-style:italic" id="clkmess">Then Click the Blinking Button That will Appear</span></p>');
							$('#description').css('color','green');
							$('#description').css('font-size','1.8em');
							$('#description').css('padding','20px');
							$('#description').show();
						});    

                       // Set up description displays for each document and button display
                       var menuTag = [];
                       var menuDesc = [];
                       var clickDiv = [];
                       var url1 = [];
                       
                       // Grainger ABCDE SeriesB Maintenance and Product Manual
                       menuTag[0] = '#doc1';
                       menuDesc[0] = '<span style="color:black">Grainger ABCDE SeriesB</span><p>This is a Maintenance and Product Information Manual tailored for a customers implementation of a ' +
								           ' Motor Efficiency Controller (MEC).  It is a new generation product manual.</p>'+
										   '<p>I wrote it in Adobe InDesign according to the customer\'s style rules</p><p style="color:green"><span style="color:green;font-style:italic" id="clkmess">Click Blinking Button Above</span></p>';

					   clickDiv[0] = '#clickdiv1';
					   url1[0] = "<?php echo base_url(); ?>"+"pdf/show_graingerABCDE.php";
					   //twMenus(menuTag[0], menuDesc[0], clickDiv[0], url1[0]);
		 
		 
		               // Grainger CDE SeriesB Maintenance and Product Manual
                       menuTag[1] = '#doc2';
                       menuDesc[1] = '<span style="color:black">Grainger CDE</span><p>This is a Maintenance and Product Information Manual tailored for a customers implementation of a ' +
								           ' Motor Efficiency Controller (MEC).  It is a new generation product manual.</p>'+
										   '<p>I wrote it in Adobe InDesign according to the customer\'s style rules</p><p style="color:green" ><span style="color:green;font-style:italic" id="clkmess">Click Blinking Button Above</span></p>';

					   clickDiv[1] = '#clickdiv2';
					   url1[1] = "<?php echo base_url(); ?>" + "pdf/show_graingerCDE.php";
					   //twMenus(menuTag[1], menuDesc[1], clickDiv[1], url1[1]);	
		 
						
					   // MEC Product Manual VT 1.6 Manual
                       menuTag[2] = '#doc3';
                       menuDesc[2] = '<span style="color:black"></span><p>This is a Product Manual for a ' +
								           ' Motor Efficiency Controller (MEC).  It is a new generation product manual.</p>'+
										   '<p>I wrote it in Adobe InDesign according to the customer\'s style rules</p><p style="color:green"><span style="color:green;font-style:italic" id="clkmess">Click Blinking Button Above</span></p>';

					   clickDiv[2] = '#clickdiv3';
					   url1[2] = "<?php echo base_url(); ?>" + "pdf/show_graingerCDE.php";
					   //twMenus(menuTag[2], menuDesc[2], clickDiv[2], url1[2]);	
						
						
						
					   // MEC White Paper
                       menuTag[3] = '#doc4';
                       menuDesc[3] = '<span style="color:black">MEC White Paper</span><p>This is a revision of the White Paper for the ' +
								           ' Motor Efficiency Controller (MEC).  It contained a fuller explanation of the electrical induction of power and torque' + 
										   ' in an electrical motor.<p>It was written in Microsoft Word.</p><p style="color:green"><span style="color:green;font-style:italic" id="clkmess">Click Blinking Button Above</span></p>';

					   clickDiv[3] = '#clickdiv4';
					   url1[3] = "<?php echo base_url(); ?>"+"pdf/show_whitePaper.php"
					   //twMenus(menuTag[3], menuDesc[3], clickDiv[3], url1[3]);			
					
							
					   // Improvement Plan Requirements
                       menuTag[4] = '#doc5';
                       menuDesc[4] = '<span style="color:black">Improvement Plan Requirements</span><p>This is technical specification of the' + 
								           ' requirements of approval and methodology of granting approval for land improvement in Clark County, as it pertains to water' + 
										   ' and sewer utility construction.</p><p>It was written in Microsoft Word.</p><p style="color:green"><span style="color:green;font-style:italic" id="clkmess">Click Blinking Button Above</span></p>';

					   clickDiv[4] = '#clickdiv5';
					   url1[4] = "<?php echo base_url(); ?>"+"pdf/show_engSpec.php";
					   //twMenus(menuTag[4], menuDesc[4], clickDiv[4], url1[4]);		
					
					
					   // Code Development Technical Writing
                       menuTag[5] = '#doc6';
                       menuDesc[5] = '<span style="color:black">Code Development Technical Writing</span><p>This is a technical specification for' + 
								      ' guidance in developing Object Oriented Code.' + '</p><p>It was written as a HTML document.</p><p style="color:green"><span style="color:green;font-style:italic" id="clkmess">Click Blinking Button Above</span></p>';

					   clickDiv[5] = '#clickdiv6';
					   url1[5] = null;
					   //twMenus(menuTag[5], menuDesc[5], clickDiv[5], url1[5]);	
		
					   for(var i=0;i<menuTag.length;i++)
							twMenus(menuTag[i], menuDesc[i], clickDiv[i], url1[i]);		
		
					
						$('#oframebg').css('background-image','url("<?php echo $imgfile2; ?>")');
						//Set up Blinking Button and span
									setInterval(function(){
									
														$('.clickdiv')	
															.animate({opacity:'0.5'},500)
															.animate({opacity:'1.0'},500);
														
														$('#clkmess')	
															.animate({opacity:'0.5'},500)
															.animate({opacity:'1.0'},500);
													},25);	

		   });

          function twMenus(menuTag, menuDesc, clickDiv, url1){
          		$(menuTag).bind('mouseover',function()
							{
								$('#description').css('color','brown');
								$('#description').css('font-size','1.4em');
								$('#description').css('text-align','left');
								$('#description').show();
								$('#description').html(menuDesc);
								$(clickDiv).show();
						  
	                           if(menuTag == '#doc6'){
	                           		 $(clickDiv).bind('click',function()
											{
			                           		   $(menuTag).css('background','linear-gradient(linen,tan)');
											   $('#description').html('Please Wait For Loading...');
											   $("#itemform input[name='itemchoice']").val('codetech');
											   $("#itemform input[name='itemchoice2']").val('none');
											   $("#itemform input[name='appType']").val('desktop');
		                                       $('#itemform').submit();	
											}); 
	                           }
	                           else if(menuTage != '#doc6'){
									  $(clickDiv).bind('click',function()
											{
											   $(menuTag).css('background','linear-gradient(linen,tan)');
											   $('#description').html('Please Wait For Loading...');
											   window.location.assign(url1);
											}); 
	                        		}
	                        		
							 $(menuTag).mouseleave(function()
									{
										
										//$(clickDiv).hide();
									});
									
							});
          	
          	
          }
        
          function JQFunctions()
                {   
                  
               }    // end of JQFunction() function
                
       
        </script>
       
		<div class="oframe" id="oframe">
			<div id="oframebg"></div>
			<div id="head">Technical Writing Documents</div>
			<div id="buttondiv">
				<div id="doc1" class="docbuttons">Grainger ABCDE<br/>SeriesB<div id="clickdiv1" class="clickdiv" >Click Here</div></div>
				<div id="doc2" class="docbuttons">Grainger<br/>CDE<div id="clickdiv2" class="clickdiv">Click Here</div></div>
				<div id="doc3" class="docbuttons">MEC Product<br/>Manual VT 1.6<div id="clickdiv3" class="clickdiv">Click Here</div></div>
				<div id="doc4" class="docbuttons">MEC<br/>White Paper<div id="clickdiv4" class="clickdiv">Click Here</div></div>
				<div id="doc5" class="docbuttons">Improvement Plan<br/>Requirements<div id="clickdiv5" class="clickdiv">Click Here</div></div>
				<div id="doc6" class="docbuttons">Code Development<br/>Technical Writing<div id="clickdiv6" class="clickdiv">Click Here</div></div>
			</div>
			<div id="description">Hover over Document Title for its Descripition.<p><p><span style="color:green;font-style:italic" id="clkmess">Then Click the Blinking Button That will Appear</span></p></div>
		 </div>
       
</div>
	
	