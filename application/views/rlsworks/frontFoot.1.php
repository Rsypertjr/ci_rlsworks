	<script type="text/javascript">	
  
			$(document).ready(function()
				{
				   // Press Footer Link for Viewing Resume
					$('#quals').bind('click',function()
						{
							$("#itemform input[name='itemchoice']").val('viewPDF');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();
							
						}); 
			
		
					
				  // Press Footer Link for Sending Email
					$('#em1').bind('click', function()
						{
							$("#itemform input[name='itemchoice']").val('email1');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();
						});	
						
				
			
				 // Press Footer Link for Motif Input Program (Amino Acid Code Sequence Analyzer)
					$('#aminoCode').bind('click', function()
						{
							$("#itemform input[name='itemchoice']").val('miniMotif');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();	
						});		
			
				
				// Press Footer Link for Othello Game
				   $('#othelloCode').bind('click', function()
					   {	
							$("#itemform input[name='itemchoice']").val('othello');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();	
						});		
				
						
					
				// Press Footer Link for Orominer Program (Human Organ System Organizer 1)
				   $('#organsCode1').bind('click', function()
						{
							$("#itemform input[name='itemchoice']").val('orominer');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();	
						
						});
			
						
				// Press Footer Link for Orominer Program with Histology(Human Organ System Organizer 2)
				   $('#organsCode2').bind('click', function()
						{
							$("#itemform input[name='itemchoice']").val('oroHist');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();	
						
						});
			
				 //Press Footer link for Dynamic Resume
				 $('#dynquals').bind('click',function()
					{
							$("#itemform input[name='itemchoice']").val('resume');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();	
					 });	
		
		         $('#quals').bind('click',function()
					{
							$("#itemform input[name='itemchoice']").val('EngExp');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();	
					 });			 
							
				 //Press Footer Link for Code Development Tech Writing
				 $('#codeDevSpec').bind('click',function()
					{
						   $("#itemform input[name='itemchoice']").val('codetech');
						   $("#itemform input[name='itemchoice2']").val('none');
						   $('#itemform').submit();	
					});
					
				
				//Press Footer Link for Improvement Plan
				$('#engProcess').bind('click',function()
					{
						  	$(this).css('color','yellow');	
							$("#itemform input[name='itemchoice']").val('engSpec');
							$("#itemform input[name='itemchoice2']").val('none');
							$('#itemform').submit();
					});
				 
				 
				 //Press Footer Link for White Paper
				 $('#whitePaper').bind('click',function()
					{
						 $("#itemform input[name='itemchoice']").val('whitePaper');
						 $("#itemform input[name='itemchoice2']").val('none');
						 $('#itemform').submit();
					});
					
				//Press Footer Link for MEC Product Manual
				 $('#tm3').bind('click',function()
					{
						$(this).css('color','yellow');	
						$("#itemform input[name='itemchoice']").val('mecProductManual');
						$("#itemform input[name='itemchoice2']").val('none');
						$('#itemform').submit();
					});
					
					
				//Press Footer Link for Grainger CDE
				$('#tm2').bind('click',function()
					{
						$(this).css('color','yellow');	
						$("#itemform input[name='itemchoice']").val('GraingerCDE');
						$("#itemform input[name='itemchoice2']").val('none');
						$('#itemform').submit();
					});
					
				
				//Press Footer Link for Grainger ABCDE Series B
				$('#tm1').bind('click',function()
					{
						$(this).css('color','yellow');	
						$("#itemform input[name='itemchoice']").val('GraingerABCDE');
						$("#itemform input[name='itemchoice2']").val('none');
						$('#itemform').submit();
					});
					
					
										
		/*		  //Dynamically Adjust Footer Font Sizes
								
					var hmHeight = $('#footContainer').height();
					var fSize = 0.032*hmHeight;
				    $('#footContainer').css('font-size',fSize+'px');	*/
		
			});  
								
</script>	
	<div id="footContainer" class="ui-widget ui-state-default ui-corner-all">
	<div id="formcontainer" style="visibility:hidden">
				<?php echo validation_errors(); ?>
				<?php 
				$attributes = array('id'=>'itemform');
				echo form_open($indexTwo,$attributes); 
				?>
						  <input type="hidden" name="itemchoice" value='none'/>
						  <input type="hidden" name="itemchoice2" value='none'/>
						  <input type="hidden" name="appType" value='<?php echo $appType; ?>'/>
				</form>
	</div>



	
		<div id="footOuter" class="ui-widget">
				<div id="softwareDevWork" class="lev1 ui-state-default">
					<div class="fTitle">Software Development</div>
					<span class="ui-icon ui-icon-triangle-1-s"></span>
				 	<div id="sdFrame" class="lev2f">
						<div id="aminoCode"  class="lev3">Amino Acid Code Sequence Analyzer</div>
						<div id="othelloCode" class="lev3" >Play Othello Game</div>
						<div id="organsCode1" class="lev3" >Human Organ System Analyzer 1</div>
						<div id="organsCode2" class="lev3" >Human Organ System Analyzer 2</div>
					</div>  
				</div>
				<div id="technicalWritingWork" class="lev1 ui-state-default">
					<div class="fTitle">Technical Writing</div>
					<span class="ui-icon ui-icon-triangle-1-s"></span>	
					<div id="twFrame" class="lev2f">
						<div id="techManuals" class="lev3" >
							<div class="fTitle">Product and Maintenance Manuals</div>
							<span class="ui-icon ui-icon-triangle-1-s"></span>
							<div id="tm1" class="lev4" >Grainger ABCDE Series B</div>
							<div id="tm2" class="lev4" >Grainger CDE</div>
							<div id="tm3" class="lev4" >MEC Product Manual VT 1.6</div>
						</div>
						<div id="whitePaper" class="lev3" >White Paper</div>
						<div id="engProcess" class="lev3" >Engineering Specification</div>
						<div id="codeDevSpec" class="lev3" >Code Development Specification</div>
					</div>
				</div>
				<div id = "engineeringWork" class="lev1 ui-state-default">
					<div class="fTitle">Engineering</div>
					<span class="ui-icon ui-icon-triangle-1-s"></span>
					<div id="ewFrame" class="lev2f">
						<div id="quals" class="lev3" >Education and Experience</div>
						<div id="dynquals" class="lev3" >Dynamic Resume</div>
					</div>
				</div>
				<div id = "contacts" class="lev1 ui-state-default">
					<div class="fTitle">Contact Me</div>
					<span class="ui-icon ui-icon-triangle-1-s"></span>	
					<div id="emFrame" class="lev2f">
						<div id="em1" class="lev3" >Send Email</div>
					</div>
				</div>
		</div>
		
	</div>

</body>
</html>
